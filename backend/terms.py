#!/usr/bin/env python3


import os

from dotenv import load_dotenv, find_dotenv
from pymongo import MongoClient

from flask import request


load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")

SOURCE = {
    "id": 1,
    "name": "Electron and/or Ion Microscopy Glossary",
    "slug_name": "em",
    "source": "Gitlab Source",
    "url": "https://codebase.helmholtz.cloud/em_glossary/em_glossary",
}


def get_terms_collection():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["terms"]


def get_all_terms():
    collection = get_terms_collection()
    page = request.args.get("page", 1, type=int)
    per_page = request.args.get("per_page", 20, type=int)
    offset = (page - 1) * per_page
    total_documents = collection.count_documents({})
    result = collection.find().sort("label_lowercase").skip(offset).limit(per_page)

    data = {
        "page": page,
        "per_page": per_page,
        "total_documents": total_documents,
        "result": list(result),
    }
    return data


def get_terms_by_list_slug(slug):
    collection = get_terms_collection()
    page = request.args.get("page", 1, type=int)
    per_page = request.args.get("per_page", 20, type=int)
    offset = (page - 1) * per_page
    total_documents = collection.count_documents(
        {"label_lowercase": {"$regex": "^" + slug}}
    )
    result = (
        collection.find({"label_lowercase": {"$regex": "^" + slug}})
        .sort("label_lowercase")
        .skip(offset)
        .limit(per_page)
    )
    data = {
        "page": page,
        "per_page": per_page,
        "total_documents": total_documents,
        "result": list(result),
    }
    return data


def get_term_by_slug(slug):
    collection = get_terms_collection()
    result = collection.find_one({"url_slug": slug})
    collection.update_one({"url_slug": slug}, {"$inc": {"view_count": 1}})
    data = {"result": result}
    return data


def get_term_understand_example(slug):
    collection = get_terms_collection()
    result = collection.find_one({"url_slug": slug})
    data = {"result": result}
    return data


def get_terms_by_query(query):
    collection = get_terms_collection()
    keywords = query.split()
    query = {"label_lowercase": {"$regex": ".*?" + keywords[0] + ".*?"}}
    for keyword in keywords[1:]:
        query["label_lowercase"]["$regex"] += ".*?" + keyword + ".*?"
    result = collection.find(query).sort("label_lowercase")
    data = {"result": list(result)}
    return data


def get_terms_and_source_statistics():
    collection = get_terms_collection()
    total_documents = collection.count_documents({})
    pipeline = [
        {
            "$group": {
                "_id": None,
                "total_view": {"$sum": "$view_count"},
                "min_date": {"$min": "$hmc_glossary_metadata.source_update_datetime"},
                "max_date": {"$max": "$hmc_glossary_metadata.source_update_datetime"},
            }
        }
    ]

    aggregate_result = list(collection.aggregate(pipeline))
    min_date = aggregate_result[0]["min_date"].strftime("%Y-%m-%d")
    max_date = aggregate_result[0]["max_date"].strftime("%Y-%m-%d")
    total_views = aggregate_result[0]["total_view"]

    data = {
        "total_terms": total_documents,
        "total_views": total_views,
        "min_date": min_date,
        "max_date": max_date,
        "source": SOURCE,
    }
    return data
