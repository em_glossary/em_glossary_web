#!/usr/bin/env python3


import os
from datetime import datetime, timedelta, timezone

from dotenv import load_dotenv, find_dotenv
from pymongo import MongoClient, DESCENDING

from terms import get_terms_collection

load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")

N_WEEK = 10
NUMBER_WEEKS = 8
FILTER_TERM_LIST = [
    "electron beam",
    "electron diffraction",
    "electron diffraction pattern",
    "ion beam",
    "time period",
]
#########
UPDATE_ALLOW = True
START_AT_CURRENT_WEEK=False

def _get_start_end_week_date(current_date):
    # Calculate the start of the week (Monday)
    start_of_week = current_date - timedelta(days=current_date.weekday())

    # Calculate the end of the week (Sunday)
    end_of_week = start_of_week + timedelta(days=6)

    # Set the time to 00:00:00 for both start and end dates
    start_of_week = start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)
    end_of_week = end_of_week.replace(hour=23, minute=59, second=59, microsecond=999999)

    return start_of_week, end_of_week

def get_word_of_the_week_collection():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["word_of_the_week"]


def get_word_of_the_week_list():
    collection = get_word_of_the_week_collection()
    result = collection.find()

    data = {
        "result": list(result),
    }
    return data


def get_current_word_of_the_week():
    # Get the current date
    current_date = datetime.now()

    start_of_week, end_of_week = _get_start_end_week_date(current_date)

    result = list(get_word_of_the_week_collection().find({"date": {'$gte': start_of_week, '$lte': end_of_week}}).sort("date", DESCENDING).limit(1))
    if result:
        term = get_terms_collection().find_one({"_id": result[0]["term_id"]})
        data = {
            "result": term,
        }
        return data
    return {}


def get_past_to_current_word_of_the_week():
    """
    return List of the word of the week form past to current date
    :return:
    """
    current_date = datetime.now()

    start_of_week, end_of_week = _get_start_end_week_date(current_date)

    result = list(get_word_of_the_week_collection().aggregate([
        { "$match": {"date": {'$lte': end_of_week}} },
        {"$lookup": {"from": "terms",
            "localField": "term_id",
            "foreignField": "_id",
            "as": "term_info"
        }
        },
        {
            "$unwind": "$term_info"
        },
        {
            "$project": {
                "term_info._id": 0,
                "term_info.comments": 0,
                "term_info.contributors": 0,
                "term_info.hmc_cross_ref": 0,
                "term_info.hmc_glossary_metadata": 0,
                "term_info.next_term": 0,
                "term_info.previous_term": 0,
                "term_info.numerical_order": 0,
                "term_info.subclassOf": 0,
            }
        },
        {
            '$sort': {'date': -1}
        },
        {
            "$limit": 16  # Limit the number of results to 16
        }
    ]))
    if result:
        return result
    return {}





def save_new_word_of_the_week(term_id, g_date=datetime.now(tz=timezone.utc)):

    collection = get_word_of_the_week_collection()
    result = {}

    term = get_terms_collection().find_one({"_id": term_id})
    # pprint.pprint(term)
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": g_date,
    }

    inserted_word_id = collection.insert_one(word_of_the_week).inserted_id
    if inserted_word_id:
        result = collection.find_one(inserted_word_id)

    return result


def _update_word_of_the_week(term_id, g_date=datetime.now(tz=timezone.utc)):
    collection = get_word_of_the_week_collection()

    start_of_week, end_of_week = _get_start_end_week_date(g_date)

    last_word_of_the_week = collection.find({"date": {'$gte': start_of_week, '$lte': end_of_week}}).sort("date", DESCENDING).limit(1)[0]
    term = get_terms_collection().find_one({"_id": term_id})
    print(
        f"Last word of the week was: ({last_word_of_the_week['label']}), "
        f"and update to: ({term['label']})"
    )
    word_of_the_week = {
        "term_id": term_id,
        "label": term["label"],
        "definition": term["definition"],
        "url_slug": term["url_slug"],
        "date": g_date,
    }

    return collection.update_one(
        {"_id": last_word_of_the_week["_id"]}, {"$set": word_of_the_week}
    )


def _select_random_term():
    pipeline = [
        {"$sample": {"size": 1}},
    ]
    selected_term = list(get_terms_collection().aggregate(pipeline))[0]
    # pprint.pprint(selected_term)
    return selected_term


def _is_word_exist_for_current_week(g_date=datetime.now(tz=timezone.utc)):
    # Get the current date
    current_date = g_date

    start_of_week, end_of_week = _get_start_end_week_date(current_date)

    word_count = get_word_of_the_week_collection().count_documents(
        {"date": {'$gte': start_of_week, '$lte': end_of_week}}
    )
    print(f"Count of word of the week for the {current_date}  : is :  {word_count}")
    print(f"Start of the week {start_of_week}")
    print(f"End of the week {end_of_week}")
    if word_count > 0:
        return True
    else:
        return False


def _count_word_of_the_week_in_n_week(label):
    today = datetime.now()
    date_n_week = today - timedelta(weeks=10)

    word_count = get_word_of_the_week_collection().count_documents(
        {"label": label, "date": {"$gte": date_n_week}}
    )

    return word_count


def _generate_new_word_of_the_week(update, g_date=datetime.now(tz=timezone.utc)):
    selected_term = _select_random_term()
    label = selected_term["label"]
    term_id = selected_term["_id"]
    print(f"The suggested random term: ({label})")
    if label in FILTER_TERM_LIST:
        print("The term is in the filtered list.")
        _generate_new_word_of_the_week(update, g_date)
    else:
        print("The term is not in the filtered list.")
        print(
            "Now we will go for next step to check this term should not be in N-LAST word of the week!"
        )

        count_word = _count_word_of_the_week_in_n_week(label)
        print(f"Last N week count: {count_word}")

        if count_word > 0:
            print(f"The {label} term exit on last {N_WEEK} week!")
            _generate_new_word_of_the_week(update, g_date)
        else:
            print(f"The {label} term NOT exit on last {N_WEEK} week!")
            if update:
                data = _update_word_of_the_week(term_id, g_date)
                print(f"Successfully UPDATE the word of the week")
            else:
                data = save_new_word_of_the_week(term_id, g_date)
                print(f"Successfully SAVE the word of the week")
            return data


def generate_new_word_of_the_week(g_date=datetime.now(tz=timezone.utc)):
    word_exit = _is_word_exist_for_current_week(g_date)

    if word_exit:
        print(f"Word for the current week exist status {word_exit}")
        print("Word of the week already exist!")

        if UPDATE_ALLOW:
            print("Update option is ON!, so we will update, if word already exist!")
            return _generate_new_word_of_the_week(True, g_date)
        print(
            "Update option is OFF!, just exit without effecting the current word of the week!"
        )
        return None
    else:
        print("Word of the week do not exist!, so create it!")
        return _generate_new_word_of_the_week(False, g_date)


def generate_list_of_word_of_the_week(number_of_week, start_at_current_week=True):
    # Get the current date
    current_date = datetime.now()

    if start_at_current_week:
        print(f"Current date {current_date}")
        weekday = current_date.weekday()
        print(f"Weekday {weekday}")
        if weekday > 0:
            current_date = current_date - timedelta(days=weekday)
            print(f"Updated current date {current_date}")

    # Initialize a counter for the loop
    count = 0

    # Loop 8 times
    while count < number_of_week:
        # Check if the current date is a Monday
        if current_date.weekday() == 0:
            # Print the Monday's date
            print(f"Week {count + 1}: {current_date}")
            print("###############################################################")
            result = generate_new_word_of_the_week(g_date=current_date)
            print("###############################################################")

            count += 1

        # Move to the next day
        current_date = current_date + timedelta(days=1)



if __name__ == "__main__":
    # generate_new_word_of_the_week()
    generate_list_of_word_of_the_week(NUMBER_WEEKS, START_AT_CURRENT_WEEK)
