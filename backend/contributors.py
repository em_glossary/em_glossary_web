#!/usr/bin/env python3


import os

from dotenv import load_dotenv, find_dotenv
from pymongo import MongoClient, DESCENDING


load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")


def get_contributors_collection():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    return db["contributors"]


def get_contributors_list():
    collection = get_contributors_collection()
    result = collection.find(
        {"display": 1, "count_terms_contribute": {"$gte": 10}}
    ).sort("count_terms_contribute", DESCENDING)

    data = {
        "result": list(result),
    }
    return data
