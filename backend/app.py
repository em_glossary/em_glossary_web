import json
from flask import Flask, jsonify
from flask_cors import CORS
from bson import json_util

from word_of_the_week import get_current_word_of_the_week, get_past_to_current_word_of_the_week
from contributors import get_contributors_list
from terms import (
    get_all_terms,
    get_terms_by_list_slug,
    get_term_by_slug,
    get_term_understand_example,
    get_terms_by_query,
    get_terms_and_source_statistics,
)

# app instance
app = Flask(__name__)
CORS(app)


# /api/home
@app.route("/api/home", methods=["GET"])
def return_home():
    return jsonify(
        {
            "message": "Welcome",
            "people": ["Mojeeb", "Özlem", "Volker", "Oonagh"],
        }
    )


# /api/terms
@app.route("/api/terms", methods=["GET"])
def return_terms():
    return json.dumps(get_all_terms(), default=json_util.default)


@app.route("/api/terms/<slug>", methods=["GET"])
def return_terms_list(slug):
    return json.dumps(get_terms_by_list_slug(slug), default=json_util.default)


@app.route("/api/term/<slug>", methods=["GET"])
def return_term(slug):
    return json.dumps(get_term_by_slug(slug), default=json_util.default)


@app.route("/api/term_understand_example/<slug>", methods=["GET"])
def return_term_understand_example(slug):
    return json.dumps(get_term_understand_example(slug), default=json_util.default)


@app.route("/api/search_terms/<query>", methods=["GET"])
def return_search_terms(query):
    return json.dumps(get_terms_by_query(query), default=json_util.default)


@app.route("/api/statistics", methods=["GET"])
def return_statistics():
    return json.dumps(get_terms_and_source_statistics(), default=json_util.default)


@app.route("/api/contributors", methods=["GET"])
def return_contributors():
    return json.dumps(get_contributors_list(), default=json_util.default)


@app.route("/api/current_word_of_the_week", methods=["GET"])
def return_current_word_of_the_week():
    return json.dumps(get_current_word_of_the_week(), default=json_util.default)

@app.route("/api/past_to_current_word_of_the_week", methods=["GET"])
def return_past_to_current_word_of_the_week():
    return json.dumps(get_past_to_current_word_of_the_week(), default=json_util.default)




if __name__ == "__main__":
    app.run(debug=False, port=5000)
