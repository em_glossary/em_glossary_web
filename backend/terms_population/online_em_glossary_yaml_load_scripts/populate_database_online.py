#!/usr/bin/env python3

import git
import os

import yaml
from dotenv import load_dotenv, find_dotenv
from pymongo import MongoClient, ASCENDING, TEXT
from slugify import slugify

from ensure_schema_compliance_online import are_terms_valid
from contributors import contributors

load_dotenv(find_dotenv())

DB_CONTAINER_NAME_OR_ADDRESS = os.environ.get("DB_CONTAINER_NAME_OR_ADDRESS")
DB_NAME = os.environ.get("DB_NAME")
TERMS_REPO_ZIP_URL = os.environ.get("TERMS_REPO_ZIP_URL")
TERMS_DIR_PATH = os.environ.get("TERMS_DIR_PATH")
SCHEMA_DIR_PATH = os.environ.get("SCHEMA_DIR_PATH")


# Clone the repository
repo = git.Repo.clone_from(
    os.environ.get("TERMS_REPO_GIT"),
    "temp_em_glossary_repo",
)


# Get the main branch
main_branch = repo.heads.main

# Checkout the main branch
repo.git.checkout(main_branch)

# Get the path to the directory containing YAML files
yaml_dir = os.path.join(repo.working_tree_dir, TERMS_DIR_PATH)

schema_file = os.path.join(repo.working_tree_dir, SCHEMA_DIR_PATH, "termSchema.json")

print(schema_file)
latest_commit_list = {}


SOURCE = {
    "id": 1,
    "name": "Electron and/or Ion Microscopy Glossary",
    "slug_name": "em",
    "source": "Gitlab Source",
    "url": "https://codebase.helmholtz.cloud/em_glossary/em_glossary",
}


validation_passed = False

print("Population Script Started!")


def set_db_index():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    collection = db["terms"]
    collection.create_index([("iri", ASCENDING)], unique=True)
    collection.create_index([("url_slug", ASCENDING)], unique=True)
    collection.create_index([("label", ASCENDING)])
    collection.create_index([("label_lowercase", ASCENDING)])
    collection.create_index([("definition", TEXT)], default_language="english")

    contributors_collection = db["contributors"]
    contributors_collection.create_index([("orcid", ASCENDING)], unique=True)

    print("The indexes is added successfully for terms collection.")
    print(sorted(list(collection.index_information())))
    client.close()
    return True


def check_db_has_this_term(iri, terms_collection):
    result = terms_collection.find_one({"iri": iri})
    return result is not None


def insert_term(term_data, terms_collection, last_modified_date):
    hmc_metadata = {
        "hmc_glossary_metadata": {
            "source": SOURCE,
            "source_update_datetime": last_modified_date,
            # "source_update_datetime": datetime.datetime.now(tz=datetime.timezone.utc),
        }
    }
    url_slug = {"url_slug": slugify(SOURCE["slug_name"] + " " + term_data["label"])}
    system_metadata = {"view_count": 0}

    hmc_cross_ref = {"hmc_cross_ref": []}

    label_lowercase = {"label_lowercase": str(term_data["label"]).lower()}

    data = (
        term_data
        | hmc_metadata
        | url_slug
        | system_metadata
        | hmc_cross_ref
        | label_lowercase
    )
    terms_collection.insert_one(data)
    return True


def update_term(term_data, terms_collection, last_modified_date):
    hmc_metadata = {
        "hmc_glossary_metadata": {
            "source": SOURCE,
            "source_update_datetime": last_modified_date,
            # "source_update_datetime": datetime.datetime.now(tz=datetime.timezone.utc),
        }
    }
    url_slug = {"url_slug": slugify(SOURCE["slug_name"] + " " + term_data["label"])}

    hmc_cross_ref = {"hmc_cross_ref": []}

    label_lowercase = {"label_lowercase": str(term_data["label"]).lower()}

    data = term_data | hmc_metadata | url_slug | hmc_cross_ref | label_lowercase

    terms_collection.update_one({"iri": term_data["iri"]}, {"$set": data})
    print(f"The {term_data['iri']} is successfully updated!")
    return True


print("Adding the terms to database started")

# check the schema of all terms are valid
if are_terms_valid(yaml_dir, schema_file):
    validation_passed = True

if validation_passed:
    # Add unique index for iri field in terms collection
    set_db_index()
    # Iterate over the YAML files in the directory
    for filename in os.listdir(yaml_dir):
        if filename.endswith(".yaml"):
            # Read the YAML file
            with open(os.path.join(yaml_dir, filename), "r") as yaml_file:
                try:
                    term = yaml.safe_load(yaml_file)

                    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
                    db = client[DB_NAME]
                    collection = db["terms"]
                    term_label = term["label"]
                    print(term_label)
                    term_path = yaml_dir + "/" + filename
                    print(term_path)

                    if check_db_has_this_term(term["iri"], collection):
                        print("iri found, go for update the fields")

                        last_commit_date = list(
                            repo.iter_commits("main", max_count=1, paths=term_path)
                        )[0].committed_datetime

                        update_term(term, collection, last_commit_date)
                    else:
                        print("iri not found, go for adding new record")
                        last_commit_date = list(
                            repo.iter_commits("main", max_count=1, paths=term_path)
                        )[0].committed_datetime
                        insert_term(term, collection, last_commit_date)

                    client.close()

                except yaml.YAMLError as e:
                    print(f"Could not load YAML file: {term_path}\n\n")

print("Adding the terms to database finished")
print("Adding the Cross ref index started")


def update_glossary_cross_ref_index():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    collection = db["terms"]
    # load all terms form Database
    # loop of terms
    for term in collection.find():
        term_label = term["label"]
        url_slug = term["url_slug"]
        print(term_label)
        print(url_slug)
        # #find the term in other terms definition
        for term_has in collection.find(
            {
                "definition": {
                    "$regex": ".*" + term_label + ".*",
                    "$options": "i",
                }
            }
        ):
            term_has_label = term_has["label"]
            print(
                f"{term_label} term has been found in the definition of {term_has_label} term,"
                f" so we added a link to it"
            )
            # ### search in hmc_cross_ref array of the term to fine in between the text
            hmc_cross_ref = term_has["hmc_cross_ref"]
            hmc_cross_ref.append({"label": term_label, "url_slug": url_slug})

            collection.update_one(
                {"iri": term_has["iri"]}, {"$set": {"hmc_cross_ref": hmc_cross_ref}}
            )

    client.close()


update_glossary_cross_ref_index()


print("Adding numerical order index and previous / next term started")


def add_previous_term_info(previous_id, current_id):
    #    set for current term the previous term as PREVIOUS TERM
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    collection = db["terms"]

    previous = collection.find_one({"_id": previous_id})

    previous_term = {
        "previous_term": {
            "label": previous["label"],
            "id": previous_id,
            "url_slug": previous["url_slug"],
        }
    }

    data = previous_term
    collection.update_one({"_id": current_id}, {"$set": data}, upsert=True)
    print(f"The previous term info added successfully!")


def add_next_term_info(previous_id, current_id):
    #    set for previous term the current term as NEXT TERM
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    collection = db["terms"]

    current = collection.find_one({"_id": current_id})

    next_term = {
        "next_term": {
            "label": current["label"],
            "id": current_id,
            "url_slug": current["url_slug"],
        }
    }

    data = next_term
    collection.update_one({"_id": previous_id}, {"$set": data}, upsert=True)
    print(f"The next term info added successfully!")


def add_numerical_order_index_previous_next():

    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    collection = db["terms"]
    # load all terms form Database
    # loop of terms

    previous_id = None
    current_id = None
    for index, term in enumerate(collection.find().sort("label_lowercase"), start=1):
        term_label = term["label"]
        current_id = term["_id"]
        print(f"This is the current Term: {term_label} with id: {current_id}")
        # Store the numerical order

        data = {"numerical_order": index}

        collection.update_one({"_id": current_id}, {"$set": data}, upsert=True)

        if previous_id:
            print(f"This is the previous Term id: {previous_id}")
            #    set for previous term the current term as NEXT TERM
            add_next_term_info(previous_id=previous_id, current_id=current_id)
            #    set for current term the previous term as PREVIOUS TERM
            add_previous_term_info(previous_id=previous_id, current_id=current_id)

        previous_id = current_id

    client.close()


add_numerical_order_index_previous_next()


def manage_contributors_info():
    client = MongoClient("mongodb://" + DB_CONTAINER_NAME_OR_ADDRESS)
    db = client[DB_NAME]
    contributors_collection = db["contributors"]
    terms_collection = db["terms"]

    # loop for all contributors
    # get counts of contributions from terms
    # collection and save with contributor info
    for contributor in contributors:
        orcid = contributor["orcid"]
        contributor_name = contributor["name"]
        print(f"{contributor_name} with orcid {orcid}")
        contributor_count = terms_collection.count_documents({"contributors": orcid})
        print(f"Count of terms contribute: {contributor_count}")

        count_terms_contribute = {"count_terms_contribute": contributor_count}

        data = contributor | count_terms_contribute

        contributors_collection.update_one(
            {"orcid": orcid}, {"$set": data}, upsert=True
        )

    client.close()


manage_contributors_info()


# Remove the local repository

os.system(f"rmdir /s /q temp_em_glossary_repo")

# shutil.rmtree("temp_em_glossary_repo")
print("Adding the Cross ref index finished")
print("Population Script Finished!")
