contributors = [
    {
        "name": "Dr. Markus Wollgarten",
        "orcid": "https://orcid.org/0000-0003-2285-9329",
        "bio": """Markus is a physicist heading the Corelab for Correlative Microscopy and Spectroscopy at the HZB. He is doing all sorts of electron and ion microscopies ever since his diploma thesis quite some time ago. His current interests cover methods around the recently introduced direct electron detectors which he employs for research on energy materials. He thinks that research data management is key to sustainable scientific work.""",
        "image": "EMGContributors_MarkusWollgarten.png",
        "center": "Helmholtz-Zentrum Berlin für Materialien und Energie GmbH",
        "display": 1,
    },
    {
        "name": "Dr. Ing. Markus Kühbach",
        "orcid": "https://orcid.org/0000-0002-7117-5196",
        "bio": """Is a metallurgist with a background on characterization and modeling of microstructure evolution during annealing phenomena using methods from physical metallurgy, computational geometry, computational materials science, and data/information science. As a PI in the FAIRmat consortium of the German National Research Data Infrastructure as well as an active member of the NFDI-MatWerk consortium, Markus develops open-source software and metadata schemas. These focus on the research fields and scientific communities of electron microscopy, atom probe, as well as integrated computational materials engineering.""",
        "image": "EMGContributors_MarkusKuehbach.png",
        "center": "FAIRmat consortium",
        "display": 1,
    },
    {
        "name": "Dr. Steffen Brinckmann",
        "orcid": "https://orcid.org/0000-0003-0930-082X",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 1,
    },
    {
        "name": "Dr. Rossella Aversa",
        "orcid": "https://orcid.org/0000-0003-2534-0063",
        "center": "Karlsruhe Institute of Technology",
        "display": 1,
    },
    {
        "name": "Dr. Robert Wendt",
        "orcid": "https://orcid.org/0000-0001-8480-4775",
        "display": 1,
    },
    {
        "name": "Dr. Reetu Elza Joseph",
        "orcid": "https://orcid.org/0000-0002-1507-9327",
        "display": 1,
    },
    {
        "name": "Dr. Luigia Cristiano",
        "orcid": "https://orcid.org/0000-0002-1418-0984",
        "display": 1,
    },
    {
        "name": "Dr. Adrien Teurtrie",
        "orcid": "https://orcid.org/0000-0002-6004-2304",
        "display": 1,
    },
    {
        "name": "Dr. Clemens Mangler",
        "orcid": "https://orcid.org/0000-0001-5870-4658",
        "center": "University of Vienna",
        "display": 1,
    },
    {
        "name": "Dr.-Ing. Christoph Pauly",
        "orcid": "https://orcid.org/0000-0002-6368-2067",
        "bio": """Is a materials scientist from Saarland University working with Focused Ion Beam and Scanning Electron Microscopes at the Core Facility "Correlative Microscopy and Tomography" (CoMiTo). Recently, Christoph has started working with (Scanning) Transmission Electron Microscopy""",
        "image": "EMGContributors_ChristophPauly.jpg",
        "center": "Saarland University",
        "display": 1,
    },
    {
        "name": "Ashish Suri",
        "orcid": "https://orcid.org/0000-0003-4745-9735",
        "display": 1,
    },
    {
        "name": "Prof. Dr. Cécile Hébert",
        "orcid": "https://orcid.org/0000-0002-7086-1901",
        "bio": """Cécile leads the laboratory for electron spectroscopy and microscopy 
      at EPFL, focusing on advancing electron microscopy techniques. Her 
      research areas include quantitative chemical analysis and imaging in 
      transmission electron microscopy supported by advanced machine 
      learning tools.""",
        "image": "EMGContributors_CecileHebert.jpg",
        "center": "École Polytechnique Fédérale de Lausanne",
        "display": 1,
    },
    {
        "name": "Prof. Dr. Helmut Kohl",
        "orcid": "https://orcid.org/0000-0001-6534-4195",
        "bio": """Irure laboris ullamco occaecat nostrud. Non cillum enim
reprehenderit ullamco ut elit ad nostrud tempor cillum. Labore sint
incididunt ut fugiat pariatur ea est veniam duis ullamco ipsum ex.""",
        "image": "",
        "center": "Münster University",
        "display": 1,
    },
    {
        "name": "Annika Strupp",
        "orcid": "https://orcid.org/0000-0002-0070-4337",
        "display": 1,
    },
    {
        "name": "Dr. Peter Konijnenberg",
        "orcid": "https://orcid.org/0000-0002-1278-4928",
        "bio": """The focus of Peter's work experience is materials informatics as well as materials characterization hardware development. Firmly rooted in the field of electron microscopy, his former positions have been either within academia (RWTH-Aachen University and Max-Planck-Institute Düsseldorf) or the electron microscopy analyzers industry (Oxford Instruments and Bruker Nano Analytics). His main areas of interest are: electron- and X-ray diffraction (amongst which notably (3D)-EBSD), computational materials science, applied crystallography and texture research, tomography and 3D visualization, as well as detector- and in-situ hardware development. While connected to the IAS-9 of  FZJ, he is currently focusing on machine learning methods for electron microscopy.""",
        "image": "EMGContributors_PeterKonijnenberg.jpg",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 1,
    },
    {
        "name": "Dr. habil. Sandor Brockhauser",
        "orcid": "https://orcid.org/0000-0002-9700-4803",
        "center": "Humboldt-Universität zu Berlin",
        "display": 1,
    },
    {
        "name": "Prof. Dr. Rasmus R. Schröder",
        "orcid": "https://orcid.org/0009-0008-6410-7217",
        "bio": """Rasmus is an expert in cryo-electron microscopy who extended his work in recent years into the field of volume electron microscopy and the 3D visualisation of structured organic nanomaterials. His current focus is on methods development and new microscopies.""",
        "image": "EMGContributors_RasmusSchroeder.jpg",
        "center": "Universitätsklinikum, Universität Heidelberg, BioQuant",
        "display": 1,
    },
    {
        "name": "Sebastian Slawik",
        "orcid": "https://orcid.org/0000-0002-3054-8734",
        "center": "Saarland University",
        "display": 1,
    },
    {
        "name": "Alexander Clausen",
        "orcid": "https://orcid.org/0000-0002-9555-7455",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 1,
    },
    {
        "name": "Dr. Said Fathalla",
        "orcid": "https://orcid.org/0000-0002-2818-5890",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 0,
    },
    {
        "name": "Simeon Ehrig",
        "orcid": "https://orcid.org/0000-0002-8218-3116",
        "center": "Center for Advanced Systems Understanding",
        "display": 1,
    },
    {
        "name": "Dr. Oonagh Brendike-Mannix",
        "center": "Helmholtz-Zentrum Berlin für Materialien und Energie GmbH",
        "orcid": "https://orcid.org/0000-0003-0575-2853",
        "bio": """After starting out in soft matter, with a focus on small-angle 
        x-ray scattering methods, Oonagh moved to the field of metadata. Now she works 
        with a team to bridge the gap between information scientists, software engineers,
         and researchers in matter. It's a lot of fun!""",
        "image": "EMGContributors_OonaghMannix.jpg",
        "display": 1,
    },
    {
        "name": "Dr. Volker Hofmann",
        "orcid": "https://orcid.org/0000-0002-5149-603X",
        "bio": """Volker works with a team of software developers, metadata stewards and 
      information scientists at the interface of research data management
      and information systems engineering towards metadata harmonization 
      and semantic interoperability in science.""",
        "image": "EMGContributors_VolkerHofmann.png",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 1,
    },
    {
        "name": "Pedro Videgain Barranco",
        "orcid": "https://orcid.org/0000-0003-0000-4784",
        "display": 0,
    },
    {
        "name": "Dr. Özlem Özkan",
        "center": "Helmholtz-Zentrum Berlin für Materialien und Energie GmbH",
        "orcid": "https://orcid.org/0000-0003-1965-7996",
        "bio": """Özlem works as the Training Officer at Helmholtz Metadata Collaboration in Berlin, focusing on FAIR data practices and metadata 
        standardization to enhance research reproducibility
        and data accessibility. She has been actively 
        working in the research data management field 
        since 2020. She holds a PhD in Medical Informatics,
        with research focused on health and genetic 
        data protection.""",
        "image": "EMGContributors_OezlemOezkan.jpg",
        "display": 1,
    },
    {
        "name": "Ahmad Zainul Ihsan",
        "orcid": "https://orcid.org/0000-0002-1008-4530",
        "center": "Forschungszentrum Jülich GmbH",
        "display": 0,
    },
    {
        "name": "Mojeeb Rahman Sedeqi",
        "center": "HZB",
        "orcid": "https://orcid.org/0000-0002-9694-0122",
        "display": 0,
    },
]
