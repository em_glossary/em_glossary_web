import { parseISO, format } from "date-fns";

export function toSentenceCase(str) {
  let item = str;
  if (item) {
    if (Array.isArray(str)) {
      item = str.toString();
    }

    return (
      item.toLowerCase().charAt(0).toUpperCase() +
      item.slice(1).replace(/_/g, " ")
    );
  }
}

export function toDefinitionCase(str) {
  return str.length > 40 ? `${str.substring(0, 40)}...` : str;
}

export function ourDecodedURIStr(str) {
  const decodedStr = decodeURIComponent(str);
  const charStr = decodedStr.replace(/\\u([\dA-Fa-f]{4})/g, (match, p1) =>
    String.fromCharCode(parseInt(p1, 16)),
  );
  return charStr;
}

export function add_cross_ref_popover(definition, cross_ref) {
  const sortedTerms = cross_ref.filter((term, index, self) => {
    return !self.some(
      (t) => t.label.includes(term.label) && t.label.length > term.label.length,
    );
  });

  sortedTerms.forEach((term) => {
    const regex = new RegExp(`\\b${term.label}\\b`, "gi");
    definition = definition.replace(
      regex,
      `<a href=${term.url_slug}>${term.label}</a>`,
    );
  });

  // console.log(definition);
  return definition;
}

export function get_cross_ref(cross_ref) {
  const sortedTerms = cross_ref.filter((term, index, self) => {
    return !self.some(
      (t) => t.label.includes(term.label) && t.label.length > term.label.length,
    );
  });

  return sortedTerms;
}

export function linked_orcid_to_name(orcid, orcid_list) {
  const orcid_found = orcid_list.filter((item) => {
    if (item.orcid == orcid) {
      return item;
    }
  });
  if (orcid_found.length > 0) {
    // console.log(orcid_found);
    return orcid_found[0].name;
  } else {
    return orcid;
  }
}

Date.prototype.getWeekNumber = function () {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return (
    1 +
    Math.round(
      ((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7,
    )
  );
};

export function format_date(dateString) {
  // console.log(dateString);
  const date1 = parseISO(dateString);
  const date = format(date1, "LLLL d, yyyy");
  return date;
}

export function format_date_year(dateString) {
  // console.log(dateString);
  const date1 = parseISO(dateString);
  const date = format(date1, "yyyy");
  return date;
}

export function week_number_year_from_date(dateString) {
  // console.log(dateString);
  const date1 = parseISO(dateString);
  const year = format(date1, "yyyy");
  const weekNumber = date1.getWeekNumber();
  return `${weekNumber} / ${year} `;
}

export function shuffle(array) {
  let currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex > 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
}

export function view_text(view_count) {
  if ((view_count == 0) | (view_count == 1)) {
    return "View";
  } else if (view_count > 1) {
    return "Views";
  }
}

export function breakStringIntoLines(inputString, charLimit) {
  if (
    typeof inputString !== "string" ||
    typeof charLimit !== "number" ||
    charLimit <= 0
  ) {
    throw new Error(
      "Invalid input: inputString must be a string and charLimit must be a positive number.",
    );
  }

  let result = [];
  for (let i = 0; i < inputString.length; i += charLimit) {
    result.push(inputString.slice(i, i + charLimit));
  }

  return result;
}
