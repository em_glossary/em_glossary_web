export const tooltip_text: Record<string, string> = {
  show_all_terms: "Show all terms",
  all_button: "Show all terms",
  letter_buttons: "Terms starting with: ",
  browse_alphabetically: "Glossary terms organized alphabetically",
  total_terms: "Total number of unique terms",
  FAQ: "Frequently Asked Questions",
  search_bar: "Search bar",
  label: "Human-readable version of a term's name",
  definition: "Meaning of the term in English",
  internal_reference: "Related terms defined in the glossary",
  comment: "Additional information about the term",
  singular: "Singular form of the term",
  plural: "Plural form of the term",
  acronyms: "Acronyms of the term",
  abbreviations: "Shortened form of the term",
  exact_synonyms: "A synonym entirely interchangeable with the term",
  narrow_synonym:
    "A synonym which has a narrower meaning than the term",
  broad_synonym:
    "A synonym that has a similar meaning but represents a more general concept",
  related_synonym:
    "Words which have similarities without being precisely synonymous",
  sources: "URLs related to the term",
  iri: "Internationalized Resource Identifier linking to more information",
  examples: "Examples which illustrate the use or context of the term",
  contributors:
    "List of people that contributed to discussions of the term definition",
  contributor: "Link to the ORCID pages",
  ratified: "Being officially approved or ratified",
  print: "Print the current term",
  view: "Total number of views of the term",
};
