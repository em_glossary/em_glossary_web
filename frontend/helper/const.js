export const ALPHABET_LIST = [
  "All",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "0",
];

export const CONTRIBUTORS_ORCID = [
  {
    name: "Oonagh Mannix",
    orcid: "https://orcid.org/0000-0003-0575-2853",
  },
  {
    name: "Markus Wollgarten",
    orcid: "https://orcid.org/0000-0003-2285-9329",
  },
  {
    name: "Volker Hofmann",
    orcid: "https://orcid.org/0000-0002-5149-603X",
  },
  {
    name: "Markus Kühbach",
    orcid: "https://orcid.org/0000-0002-7117-5196",
  },
  {
    name: "Pedro Videgain Barranco",
    orcid: "https://orcid.org/0000-0003-0000-4784",
  },
  {
    name: "Steffen Brinckmann",
    orcid: "https://orcid.org/0000-0003-0930-082X",
  },
  {
    name: "Rossella Aversa",
    orcid: "https://orcid.org/0000-0003-2534-0063",
  },
  {
    name: "Robert Wendt",
    orcid: "https://orcid.org/0000-0001-8480-4775",
  },
  {
    name: "Reetu Elza Joseph",
    orcid: "https://orcid.org/0000-0002-1507-9327",
  },
  {
    name: "Luigia Cristiano",
    orcid: "https://orcid.org/0000-0002-1418-0984",
  },
  {
    name: "Adrien Teurtrie",
    orcid: "https://orcid.org/0000-0002-6004-2304",
  },
  {
    name: "Clemens Mangler",
    orcid: "https://orcid.org/0000-0001-5870-4658",
  },
  {
    name: "Ahmad Zainul Ihsan",
    orcid: "https://orcid.org/0000-0002-1008-4530",
  },
  {
    name: "Christoph Pauly",
    orcid: "https://orcid.org/0000-0002-6368-2067",
  },
  {
    name: "Ashish Suri",
    orcid: "https://orcid.org/0000-0003-4745-9735",
  },
  {
    name: "Cecile Hebert",
    orcid: "https://orcid.org/0000-0002-7086-1901",
  },
  {
    name: "Helmut Kohl",
    orcid: "https://orcid.org/0000-0001-6534-4195",
  },
  {
    name: "Özlem Özkan",
    orcid: "https://orcid.org/0000-0003-1965-7996",
  },
  {
    name: "Annika Strupp",
    orcid: "https://orcid.org/0000-0002-0070-4337",
  },
  {
    name: "Peter Konijnenberg",
    orcid: "https://orcid.org/0000-0002-1278-4928",
  },
  {
    name: "Sandor Brockhauser",
    orcid: "https://orcid.org/0000-0002-9700-4803",
  },
  {
    name: "Rasmus R. Schroeder",
    orcid: "https://orcid.org/0009-0008-6410-7217",
  },
  {
    name: "Sebastian Slawik",
    orcid: "https://orcid.org/0000-0002-3054-8734",
  },
  {
    name: "Alexander Clausen",
    orcid: "https://orcid.org/0000-0002-9555-7455",
  },

  {
    name: "Said Fathalla",
    orcid: "https://orcid.org/0000-0002-2818-5890",
  },

  {
    name: "Simeon Ehrig",
    orcid: "https://orcid.org/0000-0002-8218-3116",
  },

  {
    name: "Mojeeb Rahman Sedeqi",
    orcid: "https://orcid.org/0000-0002-9694-0122",
  },
];
