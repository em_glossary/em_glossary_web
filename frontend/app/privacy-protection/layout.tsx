import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Privacy Protection",
  description:
    "Electron Microscopy Glossary Explorer - Privacy Protection Page",
};

export default function PrivacyProtectionLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto ">
        <div className="m-2 flex flex-col flex-wrap  sm:m-12 sm:flex-row">
          {children}
        </div>
      </div>
    </div>
  );
}
