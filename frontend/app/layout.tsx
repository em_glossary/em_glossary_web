import "./globals.css";
import "react-tooltip/dist/react-tooltip.css";
import type { Metadata } from "next";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import ScrollToTop from "@/components/ScrollToTop";
import Banner from "@/components/Banner";

export const metadata: Metadata = {
  title: "EM Glossary Explorer",
  description: "Electron Microscopy Glossary Explorer",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Header />
        <Banner />
        {children}
        <Footer />
        <ScrollToTop />
      </body>
    </html>
  );
}
