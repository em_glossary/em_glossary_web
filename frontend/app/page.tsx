import WordOfTheWeek from "@/components/WordOfTheWeek";
import WelcomeMessage from "@/components/WelcomeMessage";
import ExploreButton from "@/components/ExploreButton";
import JoinInButton from "@/components/JoinInButton";
import ImplementButton from "@/components/ImplementButton";

export default function Home() {
  return (
    <div className="min-h-full text-primary">
      <WelcomeMessage />
      <div className="container mx-auto ">
        <div className="grid grid-cols-1 gap-1 md:m-10 md:grid-cols-3">
          <ExploreButton />
          <JoinInButton />
          <ImplementButton />
        </div>
        <WordOfTheWeek />
      </div>
    </div>
  );
}
