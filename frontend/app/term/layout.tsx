import SearchBar from "@/components/SearchBar";
import Sidebar from "@/components/Sidebar";
import JoinWithUs from "@/components/JoinWithUs";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Term",
  description: "Electron Microscopy Glossary Explorer - Term Page",
};

export default function TermLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <SearchBar />
      <div className="container mx-auto ">
        <div
          className="mb-5 flex flex-col flex-wrap rounded-lg sm:border 
        md:m-12 md:flex-row lg:flex-row"
        >
          <Sidebar />
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
