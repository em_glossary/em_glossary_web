import { get_cross_ref, ourDecodedURIStr } from "@/helper/helper";
import NoTermsFound from "@/components/TermListItem/NoTermsFound";

import TermBar from "@/components/TermListItem/TermBar";
import Definition from "@/components/TermDetails/Definition";
import InternalReference from "@/components/TermDetails/InternalReference";
import Contributors from "@/components/TermDetails/Contributors";
import SeeAlso from "@/components/TermDetails/SeeAlso";
import Sources from "@/components/TermDetails/Sources";
import Examples from "@/components/TermDetails/Examples";
import Synonyms from "@/components/TermDetails/Synonyms";
import Abbreviations from "@/components/TermDetails/Abbreviations";
import Acronyms from "@/components/TermDetails/Acronyms";
import Plural from "@/components/TermDetails/Plural";
import Singular from "@/components/TermDetails/Singular";
import Comments from "@/components/TermDetails/Comments";
import Iri from "@/components/TermDetails/Iri";
import { Suspense } from "react";
import Loading from "../loading";
import TermCitation from "@/components/TermDetails/TermCitation";
import NextAndPreviousTerm from "@/components/NextAndPreviousTerm";

export const dynamic = "force-dynamic";

export const revalidate = 0;

async function getData(slug: string) {
  const res = await fetch(process.env.BACKEND_URL + "api/term/" + slug);
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary

    console.log(res);
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page({ params }: { params: { slug: string } }) {
  const { slug } = params;
  const data = await getData(slug);

  const {
    label,
    singular,
    plural,
    acronyms,
    abbreviations,
    synonyms,
    definition,
    comments,
    iri,
    seeAlso,
    contributors,
    examples,
    sources,
    hmc_cross_ref,
    previous_term,
    next_term,
  } = data.result;

  const list_ref = get_cross_ref(hmc_cross_ref);

  return (
    <div className="print-only basis-full md:basis-3/4 lg:basis-3/4">
      <Suspense fallback={<Loading />}>
        <div className="flex flex-col flex-wrap items-center justify-between rounded-lg  bg-info/[.1] px-2 py-3 md:flex-row md:px-6">
          <h1>
            <span className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent">
              {ourDecodedURIStr(String(label).toUpperCase())}
            </span>
          </h1>

          <NextAndPreviousTerm next={next_term} previous={previous_term} />
        </div>

        {data && data.result.url_slug ? (
          <>
            <div className="flex flex-auto flex-col p-4 text-xs leading-normal text-primary md:text-base ">
              <div className="grid gap-2 md:gap-4">
                <Definition
                  definition={definition}
                  hmc_cross_ref={hmc_cross_ref}
                />
                <Comments comments={comments} />
                <div className="col-span-4">
                  <hr className="mx-auto my-4 h-1 w-48 rounded border-0 bg-primary md:my-10" />
                </div>
                <Synonyms synonyms={synonyms} />
                <Abbreviations abbreviations={abbreviations} />
                <Acronyms acronyms={acronyms} />
                <InternalReference
                  list_ref={list_ref}
                  hmc_cross_ref={hmc_cross_ref}
                />
                <Singular singular={singular} />
                <Plural plural={plural} />
                <Sources sources={sources} />
                <Examples examples={examples} />
                <SeeAlso seeAlso={seeAlso} />
                <div className="col-span-4">
                  <hr className="mx-auto my-4 h-1 w-48 rounded border-0 bg-primary md:my-10" />
                </div>
                <Iri iri={iri} />
                <Contributors contributors={contributors} />
              </div>

              <div className="flex justify-end  pt-8">
                <TermBar term={data.result} />
              </div>
            </div>
            <div className="flex items-center justify-between  rounded-lg bg-info/[.1]  px-4 py-3 sm:px-6">
              <TermCitation term={data.result} />
            </div>
          </>
        ) : (
          <NoTermsFound />
        )}
      </Suspense>
    </div>
  );
}
