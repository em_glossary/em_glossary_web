import ContributorsMessage from "@/components/ContributorsMessage";
import JoinWithUs from "@/components/JoinWithUs";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Contributors",
  description: "Electron Microscopy Glossary Explorer - Contributors Page",
};

export default function ContributorsLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto ">
        <ContributorsMessage />
        <div
          className="m-2 flex flex-col flex-wrap  sm:m-12 sm:flex-row
        "
        >
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
