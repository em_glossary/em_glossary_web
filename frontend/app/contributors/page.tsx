import ContributorListItem from "@/components/ContributorListItemMain";
import ContributorListItemBasic from "@/components/ContributorListItemBasic";
import { shuffle } from "@/helper/helper";

export const dynamic = "force-dynamic";

export const revalidate = 0;

async function getData() {
  // console.log(page);

  const res = await fetch(process.env.BACKEND_URL + "api/contributors");
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page() {
  const data = await getData();

  // console.log(data);

  return (
    <div className="basis-full ">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1>Main Contributors</h1>
      </div>
      {data &&
        shuffle(
          data.result
            .filter((v: any, i: number) => v.image)
            .map((item: any, index: number) => (
              <ContributorListItem
                key={"contributor_main" + index}
                contributor={item}
              />
            )),
        )}

      {data &&
        data.result
          .filter((v: any, i: number) => !v.image)
          .map((item: any, index: number) => (
            <ContributorListItemBasic
              key={"contributor_other" + index}
              contributor={item}
            />
          ))}
    </div>
  );
}
