"use client";

import Image from "next/image";
import { useEffect } from "react";

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <div className="container mx-auto ">
      <div
        className="m-2 flex h-96  flex-col flex-wrap justify-center 
        rounded-lg border text-primary sm:m-12"
      >
        <div className="flex justify-center ">
          <Image
            src="/images/error.png"
            alt="logo"
            width={140}
            height={30}
            className="w-52 py-4 "
          />
        </div>

        <h2 className="p-2  text-center text-2xl font-semibold">
          Something went wrong!
        </h2>
        <div className="flex justify-center ">
          <button
            className={`m-px flex rounded-lg bg-primary p-2 text-center
                   text-base text-white hover:bg-info
                   focus:bg-fucus group-hover:opacity-70`}
            onClick={
              // Attempt to recover by trying to re-render the segment
              () => reset()
            }
          >
            Try again!
          </button>
        </div>
      </div>
    </div>
  );
}
