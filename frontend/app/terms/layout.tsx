import SearchBar from "@/components/SearchBar";
import Sidebar from "@/components/Sidebar";
import JoinWithUs from "@/components/JoinWithUs";
import GettingStarted from "@/components/UnderstandingEntries";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Terms",
  description: "Electron Microscopy Glossary Explorer - Terms Page",
};

export default function TermsLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary">
      <GettingStarted />
      <SearchBar />
      <div className="container mx-auto ">
        <div
          className="flex flex-col flex-wrap rounded-lg border 
        sm:m-12 sm:flex-row"
        >
          <Sidebar />
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
