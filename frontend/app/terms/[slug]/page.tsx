import TermListItem from "@/components/TermListItem";

import Pagination from "@/components/Pagination";
import { ourDecodedURIStr } from "@/helper/helper";
import NoTermsFound from "@/components/TermListItem/NoTermsFound";
import { Suspense } from "react";
import Loading from "../loading";

export const dynamic = "force-dynamic";
export const revalidate = 0;

async function getData(slug: string, page = 1) {
  const res = await fetch(
    process.env.BACKEND_URL + "api/terms/" + slug + "?page=" + page,
  );
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page({
  searchParams,
  params,
}: {
  searchParams: { page: number };
  params: { slug: string };
}) {
  const { slug } = params;
  const { page } = searchParams;

  const data = await getData(slug, page);
  let pagination = {
    page: data.page,
    per_page: data.per_page,
    total_documents: data.total_documents,
  };

  // console.log(data);

  return (
    <div className="basis-full md:basis-3/4">
      <Suspense fallback={<Loading />}>
        <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 md:px-6">
          <h1>
            Terms started by: {ourDecodedURIStr(String(slug).toUpperCase())}
          </h1>
        </div>

        {data && data.result.length <= 0 ? <NoTermsFound /> : <></>}

        {data &&
          data.result.map((item: any, index: number) => (
            <TermListItem key={"term" + index} term={item} />
          ))}

        {pagination && (
          <Pagination
            url={"/terms/" + ourDecodedURIStr(String(slug))}
            pagination={pagination}
          />
        )}
      </Suspense>
    </div>
  );
}
