import type { Metadata } from "next";

import GettingStarted from "@/components/UnderstandingEntries";
import SearchBar from "@/components/SearchBar";
import Sidebar from "@/components/Sidebar";
import JoinWithUs from "@/components/JoinWithUs";

// const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: "EM Glossary Explorer",
  description: "Electron Microscopy Glossary Explorer",
};

export default function ExploreLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary">
      <GettingStarted />
      <SearchBar />
      <div className="container mx-auto ">
        <div
          className="m-2 flex flex-col flex-wrap rounded-lg border 
      md:m-12 md:flex-row lg:flex-row"
        >
          <Sidebar />
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
