import TermListItem from "@/components/TermListItem";
import Pagination from "@/components/Pagination";
import NoTermsFound from "@/components/TermListItem/NoTermsFound";
export const dynamic = "force-dynamic";

export const revalidate = 0;

async function getData(page = 1) {
  const res = await fetch(process.env.BACKEND_URL + "api/terms?page=" + page);
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }
  return res.json();
}

export default async function Page({
  searchParams,
}: {
  searchParams: { page: number };
}) {
  const { page } = searchParams;
  const data = await getData(page);
  let pagination = {
    page: data.page,
    per_page: data.per_page,
    total_documents: data.total_documents,
  };

  // console.dir(data);
  return (
    <div className="basis-full md:basis-3/4 lg:basis-3/4">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1>ALL TERMS</h1>
      </div>
      {data && data.result.length <= 0 ? <NoTermsFound /> : <></>}
      {data &&
        data.result.map((item: any, index: number) => (
          <TermListItem key={"term" + index} term={item} />
        ))}

      {pagination && <Pagination url={"terms"} pagination={pagination} />}
    </div>
  );
}
