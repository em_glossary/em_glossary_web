import AboutMessage from "@/components/AboutMessage";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - About",
  description: "Electron Microscopy Glossary Explorer - About Page",
};

export default function AboutLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto ">
        <AboutMessage />
        <div className="m-2 flex flex-col flex-wrap  sm:m-12 sm:flex-row">
          {children}
        </div>
      </div>
    </div>
  );
}
