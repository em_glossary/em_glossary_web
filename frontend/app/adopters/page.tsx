import AdopterItem from "@/components/AdopterItem";

const adopters_list = [
  {
    name: "NFDI FAIRmat",
    logo: "adopters/FAIRmat_new_with-text.png",
    url: "https://www.fairmat-nfdi.eu/fairmat",
    info: `The NFDI FAIRmat consortium is focused on building a research data management solution in the areas of condensed-matter physics and the chemical physics of solids. This work includes the implementation and community building around [various extensions](https://fairmat-nfdi.github.io/nexus_definitions/fairmat-cover.html) to [NeXus](https://www.nexusformat.org/). Here comprehensive domain-level ontologies [NXem](https://fairmat-nfdi.github.io/nexus_definitions/classes/contributed_definitions/NXem.html#nxem) for electron microscopy and [NXapm](https://fairmat-nfdi.github.io/nexus_definitions/classes/contributed_definitions/NXapm.html#nxapm) for atom probe tomography adopt the EM Glossary terminology via domain-specific [application definitions and base classes](https://manual.nexusformat.org/classes/index.html).`,
  },
  {
    name: "Helmholtz Joint Lab MDMC",
    logo: "adopters/MDMC.png",
    url: "https://jl-mdmc-helmholtz.de/mdmc-activities/materials-characterization/",
    info: `The Helmholtz Joint Lab MDMC focuses on [correlative characterization & advanced image analysis](https://jl-mdmc-helmholtz.de/mdmc-activities/materials-characterization/), and develops application-level [metadata schemas](https://github.com/kit-data-manager/Metadata-Schemas-for-Materials-Science) for different techniques, including Transmission and Scanning Electron Microscopy in which EM Glossary terminology is referenced.
`,
  },
  {
    name: "NFDI MatWerk",
    logo: "adopters/MatWerk.jpg",
    url: "https://nfdi-matwerk.de/",
    info: `[NFDI MatWerk](https://nfdi-matwerk.de/) develops application-level metadata in collaborative Infrastructure Use Cases and Participants Projects - such as [PP13: Tomography and Microstructure based modelling](https://nfdi-matwerk.de/participant-projects/pp13-tomography-and-microstructure-based-modelling) - that [align](https://github.com/kit-data-manager/Metadata-Schemas-for-Materials-Science/tree/main/SEM-FIB_Tomography) to EM Glossary terminology.`,
  },
  {
    name: "Platform MaterialDigital",
    logo: "adopters/material_digital.jpg",
    url: "https://www.materialdigital.de/about/",
    info: `[Platform MaterialDigital (PMD)](https://www.materialdigital.de/goals/) aims to bring together and support stakeholders from the industrial and academic sectors in sustainably implementing digitalization tasks for materials and processes. Their [PMD core ontology (PMDco)](https://materialdigital.github.io/core-ontology/) is a [mid-level ontology for materials science and engineering](https://doi.org/10.1016/j.matdes.2023.112603). This ontology is extended with [application ontologies](https://github.com/materialdigital/application-ontologies), such as the [Microscopy Ontology (MO)](https://w3id.org/pmd/mo), which aligns with the EM Glossary terminology.`,
  },
];

export default async function Page() {
  return (
    <div className="basis-full ">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
        <h1>Adopters</h1>
      </div>
      {adopters_list &&
        adopters_list.map((item: any, index: number) => (
          <AdopterItem key={"adopter_" + index} item={item} />
        ))}
    </div>
  );
}
