import AdoptersMessage from "@/components/AdoptersMessage";
import JoinWithUs from "@/components/JoinWithUs";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Adopters",
  description: "Electron Microscopy Glossary Explorer - Adopters Page",
};

export default function AdoptersLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto ">
        <AdoptersMessage />
        <div
          className="m-2 flex flex-col flex-wrap  sm:m-12 sm:flex-row
        "
        >
          {children}
        </div>
      </div>
      <JoinWithUs />
    </div>
  );
}
