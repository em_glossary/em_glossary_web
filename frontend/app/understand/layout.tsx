import JoinWithUs from "@/components/JoinWithUs";
import TopMessageUnderstandingPage from "@/components/TopMessageUnderstandingPage";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "EM Glossary Explorer - Understand",
  description: "Electron Microscopy Glossary Explorer - Understand Page",
};

export default function UnderstandLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-full text-primary ">
      <div className="container mx-auto">
        <TopMessageUnderstandingPage />
        <div
          className="m-2 flex flex-col flex-wrap  
        sm:m-12 sm:flex-row"
        >
          {children}
        </div>
      </div>

      <JoinWithUs />
    </div>
  );
}
