import UnderstandingTermExample from "@/components/UnderstandingTermExample";
import UnderstandingTextInfo from "@/components/UnderstandingTextInfo";
import Loading from "@/app/term/loading";
import { Suspense } from "react";

export const dynamic = "force-dynamic";

export const revalidate = 0;

async function getData(slug: string) {
  const res = await fetch(
    process.env.BACKEND_URL + "api/term_understand_example/" + slug,
  );
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary

    console.log(res);
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Page() {
  const data = await getData("em-diffraction");

  return (
    <div className="no-print container mx-auto">
      <Suspense fallback={<Loading />}>
        <div className="flex flex-col md:flex-row">
          <div
            className="flex min-h-full basis-full flex-col 
 md:basis-2/4"
          >
            <div
              className="flex items-center 
          justify-between  rounded-l
           bg-info/[.3] px-2  py-3 font-semibold sm:px-6"
            >
              <h1>Schema Definition</h1>
            </div>
            <UnderstandingTextInfo />
          </div>
          <div className="basis-full md:basis-2/4">
            <div
              className="flex items-center 
          justify-between  bg-info/[.3] px-2 py-3  font-semibold md:rounded-r md:px-6"
            >
              <h1>Live Term Example</h1>
            </div>
            <UnderstandingTermExample data={data} />
          </div>
        </div>
      </Suspense>
    </div>
  );
}
