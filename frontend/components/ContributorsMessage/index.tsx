import Markdown from "react-markdown";

const ContributorsMessage = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div
          className="m-2 mt-6 flex flex-col justify-start
       p-2 text-primary sm:mx-12 md:flex-row"
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
              <span>See who contributes to the EM Glossary</span>
            </h2>

            <div className="flex flex-col flex-wrap text-start">
              <p>
                The EM Glossary is a community-driven collaboration of
                scientists and domain experts from electron microscopy together with experts from the fields of metadata and
                information engineering. With our combined expertise we
                work to provide high quality, harmonized, well
                implemented, and stable terminology for electron microscopy.
                Check out who contributed below!
              </p>
              <div className="py-3">
                <Markdown>{`We are always happy to welcome new contributors to our community. [Learn](https://emglossary.helmholtz-metadaten.de/about) about how you can join in.`}</Markdown>
            </div>
          </div>
        </div>
      </div>
      </div>
    </>
  );
};

export default ContributorsMessage;
