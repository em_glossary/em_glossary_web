import { get_cross_ref, ourDecodedURIStr } from "@/helper/helper";
import NoTermsFound from "@/components/TermListItem/NoTermsFound";

import TermBar from "@/components/TermListItem/TermBar";
import Definition from "@/components/TermDetails/Definition";
import InternalReference from "@/components/TermDetails/InternalReference";
import Contributors from "@/components/TermDetails/Contributors";
import SeeAlso from "@/components/TermDetails/SeeAlso";
import Sources from "@/components/TermDetails/Sources";
import Examples from "@/components/TermDetails/Examples";
import Synonyms from "@/components/TermDetails/Synonyms";
import Abbreviations from "@/components/TermDetails/Abbreviations";
import Acronyms from "@/components/TermDetails/Acronyms";
import Plural from "@/components/TermDetails/Plural";
import Singular from "@/components/TermDetails/Singular";
import Comments from "@/components/TermDetails/Comments";
import Iri from "@/components/TermDetails/Iri";

import TermCitation from "@/components/TermDetails/TermCitation";

export default function UnderstandingTermExample({ data }: { data: any }) {
  const {
    label,
    singular,
    plural,
    acronyms,
    abbreviations,
    synonyms,
    definition,
    comments,
    iri,
    seeAlso,
    contributors,
    examples,
    sources,
    hmc_cross_ref,
  } = data.result;

  const list_ref = get_cross_ref(hmc_cross_ref);

  return (
    <div className="sm:p-1 md:border-l md:p-2 lg:p-8">
      <div className="flex items-center justify-between  rounded-lg bg-info/[.1]  py-3 sm:px-6 md:px-4">
        <h1>
          <span className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent">
            {ourDecodedURIStr(String(label).toUpperCase())}
          </span>
        </h1>
      </div>

      {data && data.result.url_slug ? (
        <>
          <div className="flex flex-auto flex-col p-1 text-xs leading-normal text-primary sm:text-base  md:p-1 lg:p-4">
            <div className="grid gap-1 md:gap-2 lg:gap-6">
              <Definition
                definition={definition}
                hmc_cross_ref={hmc_cross_ref}
              />
              <Comments comments={comments} />
              <div className="col-span-4">
                <hr className="mx-auto my-4 h-1 w-48 rounded border-0 bg-primary md:my-10" />
              </div>
              <Synonyms synonyms={synonyms} />
              <Abbreviations abbreviations={abbreviations} />
              <Acronyms acronyms={acronyms} />
              <InternalReference
                list_ref={list_ref}
                hmc_cross_ref={hmc_cross_ref}
              />
              <Singular singular={singular} />
              <Plural plural={plural} />
              <Sources sources={sources} />
              <Examples examples={examples} />
              <SeeAlso seeAlso={seeAlso} />
              <div className="col-span-4">
                <hr className="mx-auto my-4 h-1 w-48 rounded border-0 bg-primary md:my-10" />
              </div>
              <Iri iri={iri} />
              <Contributors contributors={contributors} />
            </div>

            <div className="flex justify-end  pt-8">
              <TermBar term={data.result} />
            </div>
          </div>
          <div className="flex items-center justify-between  rounded-lg bg-info/[.1]  px-4 py-3 sm:px-6">
            <TermCitation term={data.result} />
          </div>
        </>
      ) : (
        <NoTermsFound />
      )}
    </div>
  );
}
