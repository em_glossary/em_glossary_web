"use client";

import Field_list from "./Field_list";

export default function UnderstandingTextInfo() {
  return (
    <div className="no-print container mx-auto">
      <div className="m-2 rounded-lg  bg-info/[.1]">
        <Field_list />
      </div>
    </div>
  );
}
