"use client";

import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";
import Markdown from "react-markdown";
import rehypeExternalLinks from "rehype-external-links";
import remarkGfm from "remark-gfm";

const field_text: any = [
  {
    title: "Label",
    body: `The human-readable, most commonly used name for the term. In the EM Glossary OWL implementation this is used to name each class.`,
    is_mandatory: true,
  },
  {
    title: "Definition",
    body: `This phrase describes the meaning behind the Label. It is designed for human understanding. 
For clarity and brevity our definitions follow two principles:
- We use the Aristotelian [Genus-Differentia form](https://en.wikipedia.org/wiki/Genus%E2%80%93differentia_definition) for writing definitions. As such our definitions start with a genus, A,  followed by a list of differentia B1, B2... Bn. Differentia are features which differentiate this particular term from other terms of the same genus. This results in a definition in the form: term is an A which B1, B2... Bn. 
In our OWL implementation the genus gets mapped to the class hierarchy, and a subclass in the hierarchy inherits all properties from its superclass.

- We unpack definitions. Where definitions require subject-specific technical terms, we use them, but ensure that they are defined elsewhere in the glossary as well. The result is a strict one-term-per-definition policy. While browsing the glossary you will find and quick-links to these related terms and their definitions, as well as an entry under the Internal Reference key.
`,
    is_mandatory: true,
  },
  {
    title: "Contributors",
    body: `A list which identifies the persons that contributed to discussions and drafting of the content associated with each term.
     The links in the term resolve to [ORCID](https://orcid.org/) which are persistent identifiers for people.
     More information on our contributors can be found in the [contributors page](/contributors).

    `,
    is_mandatory: true,
  },
  {
    title: "IRI",
    body: `Stands for [Internationalized Resource Identifier](https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier) - a persistent, unique way of identifying the web location where a term's information is stored. It is important for the machine readability of the OWL implementation of the EM Glossary. The link will forward you to the documentation page of the EM Glossary OWL artifact.

    `,
    is_mandatory: true,
  },
  {
    title: "Comments",
    body: `Contains additional information about a term. When writing the definition this information, although important, was not considered pertinent to the features that differentiate this particular term, and therefore not included in the definition's differentia. Comments further enrich a term description with additional context.`,
    is_mandatory: false,
  },
  {
    title: "Acronyms",
    body: `A form of representing the term by its initials (or other letters), to be pronounced like a word, and be used verbally or in the literature. An example is IRI for International Resource Identifier.


    `,
    is_mandatory: false,
  },
  {
    title: "Abbreviations",
    body: `A shortened form of the word or phrase used as the term's Label. The shorter form is often used as a shortcut when writing.

    `,
    is_mandatory: false,
  },
  {
    title: "Sources",
    body: `A list of external resources used during the discussion and drafting process of the Definition, and other term information. 

    `,
    is_mandatory: false,
  },
  {
    title: "Examples",
    body: `A list of examples which are thought to illustrate the use of, or give context to, a term; thereby aiding practical application and deepening comprehension. An Example for a person would be John Doe.
    `,
    is_mandatory: false,
  },
  {
    title: "Internal reference",
    body: `A list of terms that are used in the Definition or Comment, and which are defined and used elsewhere in the EM Glossary. By clicking the links, you can easily navigate to related terminology in this glossary.

    `,
    is_mandatory: false,
  },
  {
    title: "Synonyms",
    body: `- **Exact Synonyms:** A list of synonyms that are entirely interchangeable with the term Label. The Definitions of the term Label and its Exact Synonyms are identical. 
- **Narrow Synonyms:** A list of synonyms that share meaning with the term Label, but are more specific and represent a more limited concept. The term Label and its Narrow Synonyms are not interchangeable. The term Label will have meanings that are not covered by the Narrow Synonyms.
- **Broad Synonyms:** A list of synonyms that share meaning with the term Label, but are less specific and represent a more general concept. The term Label and its Broad Synonyms are not interchangeable. The Broad Synonym will have meanings not covered by the Label.
- **Related Synonyms:** A list of synonyms that share partial meaning with the term Label. The term Label and its Related Synonyms are not interchangeable. Each have distinct meanings, but also share some overlap in their meaning.
`,
    is_mandatory: false,
  },
];

export default function Field_list() {
  return (
    <>
      {field_text.map((item: any, index: number) => (
        <Disclosure
          defaultOpen={item.is_mandatory}
          key={"field_" + index}
          as="div"
          className="mt-1"
        >
          {({ open }) => (
            <>
              <Disclosure.Button
                className="flex w-full justify-between rounded-lg
               bg-info/[.2] px-4 py-2 text-left text-base
               font-medium  hover:bg-info/[.4]
                focus:outline-none focus-visible:ring
                 focus-visible:ring-info 
                 focus-visible:ring-opacity-75 "
              >
                <span className="font-semibold">{item.title}</span>
                <span className="flex flex-row">
                  <span className="mr-4 rounded-md bg-info/[.2] p-1 text-sm font-medium">
                    {item.is_mandatory ? "Mandatory" : "Optional"}
                  </span>
                  <span>
                    <ChevronUpIcon
                      className={`${
                        open ? "rotate-180 transform" : ""
                      } h-7 w-7 rounded-md bg-info/[.2]`}
                    />
                  </span>
                </span>
              </Disclosure.Button>
              <Disclosure.Panel className="px-4 pb-2 pt-4 text-sm">
                <div className="my-markdown">
                  <Markdown
                    rehypePlugins={[
                      [rehypeExternalLinks, { target: "_blank" }],
                    ]}
                    remarkPlugins={[remarkGfm]}
                  >
                    {item.body}
                  </Markdown>
                </div>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>
      ))}
    </>
  );
}
