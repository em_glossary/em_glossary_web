"use client";

import { useRouter } from "next/navigation";
import ReactPaginate from "react-paginate";

const Pagination = ({ pagination, url }) => {
  const router = useRouter();

  return (
    <div className="flex flex-col items-center justify-between gap-6  rounded-lg bg-info/[.2]  px-4 py-3 sm:px-6">
      <div>
        <p className="text-sm text-primary">
          Active Page
          <span className="m-5 font-bold">{pagination.page}</span>
          Per Page Limit
          <span className="m-5 font-bold">{pagination.per_page}</span>
          Total Pages
          <span className=" m-5 font-bold">
            {Math.ceil(pagination.total_documents / pagination.per_page)}
          </span>
          Total Terms:
          <span className=" m-5 font-bold">{pagination.total_documents}</span>
        </p>
      </div>
      <div className="sm:flex sm:flex-1 sm:items-center sm:justify-between">
        <ReactPaginate
          breakLabel="..."
          nextLabel={
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z"
                clipRule="evenodd"
              />
            </svg>
          }
          onPageChange={(event) =>
            router.push(`${url}?page=${event.selected + 1}`)
          }
          pageRangeDisplayed={3}
          pageCount={Math.ceil(
            pagination.total_documents / pagination.per_page,
          )}
          previousLabel={
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z"
                clipRule="evenodd"
              />
            </svg>
          }
          renderOnZeroPageCount={null}
          containerClassName="isolate inline-flex -space-x-px rounded-md shadow-sm"
          previousClassName="relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
          nextClassName="relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
          disabledClassName={
            "relative inline-flex items-center border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20"
          }
          activeClassName={
            "relative z-10 inline-flex items-center border border-info bg-info px-4 py-2 text-sm font-medium text-primary focus:z-20"
          }
          pageClassName={
            "relative hidden items-center border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 focus:z-20 md:inline-flex"
          }
        />
      </div>
    </div>
  );
};

export default Pagination;
