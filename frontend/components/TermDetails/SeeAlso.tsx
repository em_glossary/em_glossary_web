"use client";

import { breakStringIntoLines } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const SeeAlso = ({ seeAlso }: { seeAlso: any }) => {
  return (
    <>
      {seeAlso && (
        <>
          <div className="tooltip_see_also col-span-1 self-center">
            <h2 className="px-3 font-bold">See Also:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(seeAlso).map(([key, value]: any) => (
              <p key={`seeAlso${key}`} className="py-1">
                <a href={value} target="_blank" rel="noopener noreferrer">
                  <span>
                    {breakStringIntoLines(value, 35).map(
                      (v: string, i: number) => (
                        <span key={`see-link${i}`}>{v + "\n"}</span>
                      ),
                    )}
                  </span>
                </a>
              </p>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_see_also`}
            place="bottom"
            content={"See Also"}
          />
        </>
      )}
    </>
  );
};

export default SeeAlso;
