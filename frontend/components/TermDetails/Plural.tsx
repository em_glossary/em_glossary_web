"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Plural = ({ plural }: { plural: any }) => {
  return (
    <>
      {plural && (
        <>
          <div className="tooltip_plural col-span-1 self-center">
            <h2 className="px-3 font-bold">Plural:</h2>
          </div>
          <div className="col-span-3">
            <h2 className="break-all text-lg font-bold ">
              {toSentenceCase(plural.plural_en)}
            </h2>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_plural`}
            place="bottom"
            content={tooltip_text["plural_en"]}
          />
        </>
      )}
    </>
  );
};

export default Plural;
