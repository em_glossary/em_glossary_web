"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Acronyms = ({ acronyms }: { acronyms: any }) => {
  return (
    <>
      {acronyms && (
        <>
          <div className="tooltip_acronyms col-span-1 self-center">
            <h2 className="px-3 font-bold ">Acronyms:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(acronyms).map(([key, value]: any) => (
              <>
                <h5 key={`acronyms_${key}`} className="font-bold ">
                  {toSentenceCase(value)}
                </h5>
              </>
            ))}
          </div>

          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_acronyms`}
            place="bottom"
            content={tooltip_text["acronyms"]}
          />
        </>
      )}
    </>
  );
};

export default Acronyms;
