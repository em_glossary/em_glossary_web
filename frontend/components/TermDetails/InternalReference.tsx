"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import Link from "next/link";
import { Tooltip } from "react-tooltip";

const InternalReference = ({
  list_ref,
  hmc_cross_ref,
}: {
  list_ref: any;
  hmc_cross_ref: any;
}) => {
  return (
    <>
      {list_ref && list_ref.length > 0 ? (
        <>
          <div className="tooltip_internal_ref col-span-1 self-center">
            <h2 className="px-3 font-bold">Internal References:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(hmc_cross_ref).map(([key, item]: any) => (
              <p key={`hmc_cross_ref_${key}`} className="text-justify">
                <Link href={`/term/${item.url_slug}`}>
                  <span className="border-linked">
                    {toSentenceCase(item.label)}
                  </span>
                </Link>
              </p>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_internal_ref`}
            place="bottom"
            content={tooltip_text["internal_reference"]}
          />
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default InternalReference;
