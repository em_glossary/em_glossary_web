"use client";

import { breakStringIntoLines } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Sources = ({ sources }: { sources: any }) => {
  return (
    <>
      {sources && (
        <>
          <div className="tooltip_sources col-span-1 self-center">
            <h2 className="px-3 font-bold">Sources:</h2>
          </div>
          <div className="col-span-3 text-xs md:text-base">
            {Object.entries(sources).map(([key, value]: any) => (
              <p key={`sources${key}`} className="py-1">
                <a href={value} target="_blank" rel="noopener noreferrer">
                  <span>
                    {breakStringIntoLines(value, 35).map(
                      (v: string, i: number) => (
                        <span key={`source-link${i}`}>{v + "\n"}</span>
                      ),
                    )}
                  </span>
                </a>
              </p>
            ))}
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_sources`}
            place="bottom"
            content={tooltip_text["sources"]}
          />
        </>
      )}
    </>
  );
};

export default Sources;
