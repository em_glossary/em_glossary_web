"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Iri = ({ iri }: { iri: any }) => {
  return (
    <>
      {iri && (
        <>
          <div className="tooltip_iri col-span-1 self-center">
            <h2 className="px-3 font-bold">IRI:</h2>
          </div>
          <div className="col-span-3 text-pretty text-xs md:text-base">
            <p className=" text-justify">
              <a
                href={iri}
                target="_blank"
                rel="noopener noreferrer"
                className="overflow-hidden text-ellipsis"
              >
                {iri}
              </a>
            </p>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_iri`}
            place="bottom"
            content={tooltip_text["iri"]}
          />
        </>
      )}
    </>
  );
};

export default Iri;
