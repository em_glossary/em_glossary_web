"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Abbreviations = ({ abbreviations }: { abbreviations: any }) => {
  return (
    <>
      {abbreviations && (
        <>
          <div className="tooltip_abbreviations col-span-1 self-center">
            <h2 className="px-3 font-bold">Abbreviations:</h2>
          </div>
          <div className="col-span-3">
            {Object.entries(abbreviations).map(([key, value]: any) => (
              <>
                <h5 key={`abbreviations_${key}`} className="font-bold">
                  {toSentenceCase(value)}
                </h5>
              </>
            ))}
          </div>

          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_abbreviations`}
            place="bottom"
            content={tooltip_text["abbreviations"]}
          />
        </>
      )}
    </>
  );
};

export default Abbreviations;
