"use client";

import { toSentenceCase } from "@/helper/helper";
import { tooltip_text } from "@/helper/tooltips_text";
import { Fragment } from "react";
import { Tooltip } from "react-tooltip";

const Synonyms = ({ synonyms }: { synonyms: any }) => {
  return (
    <>
      {synonyms && (
        <>
          {Object.entries(synonyms).map(([key, value]: [string, any]) => (
            <Fragment key={key}>
              <div className="tooltip_synonyms col-span-1 self-center">
                <h2 className="px-3 font-bold">{toSentenceCase(key)}:</h2>
              </div>
              <div className="tooltip_synonyms_detail col-span-3">
                <span key={`synonyms${key}`}>
                  <h5 className="font-bold">{toSentenceCase(value)}</h5>
                  <Tooltip
                    className="my_tooltip"
                    anchorSelect={`.tooltip_synonyms_detail`}
                    place="bottom"
                    content={tooltip_text[key]}
                  />
                </span>
              </div>
            </Fragment>
          ))}
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_synonyms`}
            place="bottom"
            content={`Synonym corresponds to the same definition.`}
          />
        </>
      )}
    </>
  );
};

export default Synonyms;
