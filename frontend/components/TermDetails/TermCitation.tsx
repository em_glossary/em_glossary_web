"use client";

import { CopyToClipboard } from "react-copy-to-clipboard";
import { MyDateYear } from "../TermListItem/MyDate";
import { useState } from "react";
import { format_date_year } from "@/helper/helper";
import Image from "next/image";
const TermCitation = ({ term }: { term: any }) => {
  const [copy, setCopy] = useState(false);
  return (
    <>
      <div className="flex flex-col justify-between  sm:flex-row">
        <div className="flex flex-row items-center  gap-x-1 text-sm text-gray-700">
          <Image
            src={`/images/icons/quotes.png`}
            alt="cite icon"
            width={32}
            height={32}
          />
          <div className="flex flex-col gap-1 pl-4">
            <p className="font-bold">Please cite as:</p>
            <div className="text-sm font-thin">
              <span className="italic">&apos;{String(term.label)}&apos; </span>
              <span> in EM-Glossary; online v1.0.0., </span>
              <span>
                <MyDateYear
                  dateString={
                    term.hmc_glossary_metadata.source_update_datetime.$date
                  }
                />
              </span>
              <span>, {String(term.iri)}</span>
            </div>
          </div>
        </div>
      </div>
      <CopyToClipboard
        text={`'${String(term.label)}' in EM Glossary; online v1.0.0., ${format_date_year(
          term.hmc_glossary_metadata.source_update_datetime.$date,
        )}, ${String(term.iri)}`}
        onCopy={() => setCopy(true)}
      >
        <button
          className={`inline-flex items-center rounded  px-2 py-2 text-center text-sm font-medium text-white hover:bg-info 
        ${copy ? " bg-green-600" : "bg-primary"}`}
        >
          {copy ? (
            <>
              {" "}
              Copied{" "}
              <svg
                className="h-6 w-6 text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M10 3v4a1 1 0 0 1-1 1H5m4 6 2 2 4-4m4-8v16a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V7.914a1 1 0 0 1 .293-.707l3.914-3.914A1 1 0 0 1 9.914 3H18a1 1 0 0 1 1 1Z"
                />
              </svg>
            </>
          ) : (
            <>
              Copy Citation{" "}
              <svg
                className="h-8 w-8 text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M9 8v3a1 1 0 0 1-1 1H5m11 4h2a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1h-7a1 1 0 0 0-1 1v1m4 3v10a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1v-7.13a1 1 0 0 1 .24-.65L7.7 8.35A1 1 0 0 1 8.46 8H13a1 1 0 0 1 1 1Z"
                />
              </svg>
            </>
          )}
        </button>
      </CopyToClipboard>
    </>
  );
};

export default TermCitation;
