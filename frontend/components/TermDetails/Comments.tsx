"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";

const Comments = ({ comments }: { comments: any }) => {
  return (
    <>
      {comments && (
        <>
          <div className="tooltip_comments col-span-1 self-center font-bold">
            <h2 className="px-3 font-bold">Comments:</h2>
          </div>
          <div className="col-span-3 font-bold">
            <p className="text-xs md:text-base">{comments}</p>
          </div>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_comments`}
            place="bottom"
            content={tooltip_text["comment"]}
          />
        </>
      )}
    </>
  );
};

export default Comments;
