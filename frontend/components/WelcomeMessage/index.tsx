const WelcomeMessage = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div
          className="m-2 mt-6 flex flex-col justify-start
       p-2 text-primary sm:mx-12 md:flex-row"
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
              <span>Welcome to the Electron Microscopy Glossary Explorer</span>
            </h2>

            <div className="flex flex-col flex-wrap text-center">
              <p>
                We work together to harmonise terminology in electron microscopy
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default WelcomeMessage;
