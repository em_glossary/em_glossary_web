"use client";

import { tooltip_text } from "@/helper/tooltips_text";
import { Tooltip } from "react-tooltip";
import FAQ_list from "./FAQ_list";

export default function FAQ() {
  return (
    <div className="no-print container mx-auto">
      <div className="m-2 rounded-lg  bg-info/[.2] sm:m-12">
        <h1 className="tooltip_FAQ px-8 py-4 text-2xl font-semibold">FAQ</h1>
        <Tooltip
          className="my_tooltip"
          anchorSelect={`.tooltip_FAQ`}
          place="bottom"
          content={tooltip_text["FAQ_tooltip"]}
        />
        <FAQ_list />
      </div>
    </div>
  );
}
