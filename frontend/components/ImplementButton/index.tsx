import Link from "next/link";

const ImplementButton = () => {
  return (
    <>
      <a
        target="_blank"
        href={`https://purls.helmholtz-metadaten.de/emg`}
        className="m-2 flex flex-col justify-start  rounded-lg border-2 
        border-primary bg-info/[.05] py-6 shadow-sm
                 hover:bg-slate-100 
                 group-hover:opacity-90
                  md:flex-row 
                  "
      >
        <div className="flex flex-auto flex-col  p-4 leading-normal ">
          <h2 className="break-all p-2  text-center text-3xl font-bold ">
            <span>IMPLEMENT</span>
          </h2>

          <p className="p-2  pb-3 text-center text-base   ">
            Implement EM Glossary terminology in your metadata with our OWL
            artifact.
          </p>

          <div className="flex justify-center">
            <button
              type="button"
              className="inline-flex items-center rounded bg-primary px-5 py-3 text-center text-base font-medium text-white hover:bg-info"
            >
              See artifact
              <svg
                className="ms-2 h-6 w-6"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeWidth="2"
                  d="M8.737 8.737a21.49 21.49 0 0 1 3.308-2.724m0 0c3.063-2.026 5.99-2.641 7.331-1.3 1.827 1.828.026 6.591-4.023 10.64-4.049 4.049-8.812 5.85-10.64 4.023-1.33-1.33-.736-4.218 1.249-7.253m6.083-6.11c-3.063-2.026-5.99-2.641-7.331-1.3-1.827 1.828-.026 6.591 4.023 10.64m3.308-9.34a21.497 21.497 0 0 1 3.308 2.724m2.775 3.386c1.985 3.035 2.579 5.923 1.248 7.253-1.336 1.337-4.245.732-7.295-1.275M14 12a2 2 0 1 1-4 0 2 2 0 0 1 4 0Z"
                />
              </svg>
            </button>
          </div>
        </div>
      </a>
    </>
  );
};

export default ImplementButton;
