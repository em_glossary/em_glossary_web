import Image from "next/image";
const TermListItem = ({ contributor }: { contributor: any }) => {
  return (
    <>
      {contributor && (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`${contributor.orcid}`}
          className="contributor m-2 flex flex-col justify-start rounded-lg border bg-white shadow-sm
            hover:bg-gray-100 md:flex-row"
        >
          <div className="basis-2/6 md:basis-2/6 lg:basis-1/6">
            <Image
              className="h-56 w-56  rounded-t-lg object-center md:h-56 md:w-56 md:rounded-none md:rounded-s-lg"
              src={`/images/contributors/${
                contributor.image != "" ? contributor.image : "default.jpg"
              }`}
              alt={contributor.name}
              width={400}
              height={400}
            />
          </div>

          <div className="flex flex-auto basis-4/6 flex-col p-4 leading-normal md:basis-4/6 lg:basis-5/6 ">
            <h2 className="break-all p-2 text-lg font-bold  text-gray-900  ">
              <span
                className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent 
               "
              >
                {contributor.name}
              </span>
            </h2>

            <div className="flex flex-row flex-wrap justify-between pb-2">
              <p className="px-2">
                <span className="px-2 pb-4 font-normal text-lime-500 ">
                  {contributor.center}
                </span>
                <span> - </span>
                <Image
                  src="/images/orcid.svg"
                  alt="pch.vector / Freepik"
                  width={30}
                  height={30}
                  className="mr-1 inline-flex w-3"
                />
                <span className="text-xs">{contributor.orcid}</span>
              </p>
              <p className="px-2">
                <span className="px-2 text-xs font-normal  ">
                  Terms Contribution Count:{" "}
                  <span className="font-bold text-lime-500">
                    {contributor.count_terms_contribute}
                  </span>
                </span>
              </p>
            </div>
            <hr />

            <div className="flex flex-col flex-wrap text-pretty py-2" dir="ltr">
              <h5 className="px-2 font-normal text-primary ">
                {contributor.bio}
              </h5>
            </div>
          </div>
        </a>
      )}
    </>
  );
};

export default TermListItem;
