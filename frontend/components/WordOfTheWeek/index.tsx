"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { ColorRing } from "react-loader-spinner";

import WordOfTheWeekItem from "../WordOfTheWeekItem";




const WordOfTheWeek = () => {
  const [data, setData] = useState<any>([]);
  const [isLoading, setLoading] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [currentData, setCurrentData] = useState({});
  const set_word_of_the_week = (index=0) => {
      console.log(index)
      console.log(data.length)
      console.log(data)
      if (index < data.length && index >= 0) {
          setCurrentData(data[index])
          setCurrentIndex(index)
      }
      console.log(currentData)

}


  useEffect(() => {
    setLoading(true);
    axios
      .get(process.env.NEXT_PUBLIC_BACKEND_URL + "api/past_to_current_word_of_the_week")
      .then((data) => {
        console.log(data.data);
        setData(data.data);
        setCurrentData(data.data[0])
        setLoading(false);

      });
  }, []);

  return (
    <>
      {isLoading && (
        <ColorRing
          visible={true}
          height="80"
          width="80"
          ariaLabel="blocks-loading"
          wrapperStyle={{}}
          wrapperClass="blocks-wrapper"
          colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]}
        />
      )}
      {data && data.length > 0 && currentData && (
        <div className="no-print container  mx-auto ">
          <div className="m-2 mb-8">
              <WordOfTheWeekItem
                  movePrevious={() => set_word_of_the_week(currentIndex + 1)}
                  moveNext={() => set_word_of_the_week(currentIndex - 1)}
                  term={currentData}
                  currentIndex={currentIndex}
                  totalIndex={data.length}
              />
          </div>
        </div>
      )}
    </>
  );
};

export default WordOfTheWeek;
