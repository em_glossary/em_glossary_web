import Image from "next/image";
import Link from "next/link";
const ReadOnBoxInfo = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div
          className="m-2 flex flex-col  flex-wrap rounded-lg 
        border bg-info/[.2] p-8 sm:m-12"
        >
          <h1 className="text-2xl font-semibold">Read on</h1>

          <p className="py-2 text-justify">
            All terms are accessible in the
            <Link href={"/terms"} className="">
              {" "}
              next page
            </Link>
            .
          </p>
        </div>
      </div>
    </>
  );
};

export default ReadOnBoxInfo;
