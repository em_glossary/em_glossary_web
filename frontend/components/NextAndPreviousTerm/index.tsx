import { toSentenceCase } from "@/helper/helper";
import Link from "next/link";

const NextAndPreviousTerm = ({
  previous,
  next,
}: {
  previous: any;
  next: any;
}) => {
  return (
    <>
      <div className="flex">
        <span>
          {previous && (
            <Link
              href={`/term/${previous.url_slug}`}
              className="flex flex-row rounded-lg py-2 
                     text-center text-xs hover:bg-primary hover:text-white sm:p-2"
            >
              <span className="flex flex-col">
                <span className="text font-semibold md:text-base">
                  Previous
                </span>
                <span className="text-sm">
                  {toSentenceCase(previous.label)}
                </span>
              </span>
              <span>
                <svg
                  className="h-10 w-10 "
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path
                    fillRule="evenodd"
                    d="M7 6a1 1 0 0 1 2 0v4l6.4-4.8A1 1 0 0 1 17 6v12a1 1 0 0 1-1.6.8L9 14v4a1 1 0 1 1-2 0V6Z"
                    clipRule="evenodd"
                  />
                </svg>
              </span>
            </Link>
          )}
        </span>

        <span>
          {next && (
            <Link
              href={`/term/${next.url_slug}`}
              className="flex flex-row rounded-lg  py-2 text-center
                     text-xs hover:bg-primary hover:text-white sm:p-2"
            >
              <span>
                <svg
                  className="h-10 w-10 "
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path
                    fill-rule="evenodd"
                    d="M17 6a1 1 0 1 0-2 0v4L8.6 5.2A1 1 0 0 0 7 6v12a1 1 0 0 0 1.6.8L15 14v4a1 1 0 1 0 2 0V6Z"
                    clip-rule="evenodd"
                  />
                </svg>
              </span>
              <span className="flex flex-col">
                <span className="text font-semibold md:text-base">Next</span>
                <span className="text-sm">{toSentenceCase(next.label)}</span>
              </span>
            </Link>
          )}
        </span>
      </div>
    </>
  );
};

export default NextAndPreviousTerm;
