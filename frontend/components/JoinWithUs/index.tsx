import Link from "next/link";

const JoinWithUs = () => {
  return (
    <>
      <div
        className="wow fadeInUp z-10  bg-info/[.1]  hover:bg-slate-100"
        data-wow-delay=".1s"
      >
        <Link href={`/about#join-us`} className="m-0">
          <div className="grid grid-cols-1 items-center py-6 sm:grid-cols-6">
            <h2 className="px-2 text-2xl font-bold sm:px-8">Join us!</h2>
            <p className="p-2 text-lg font-semibold sm:col-span-5">
              Find out how you can contribute to our term definitions
            </p>
          </div>
        </Link>
      </div>
    </>
  );
};

export default JoinWithUs;
