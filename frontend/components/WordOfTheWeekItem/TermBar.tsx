"use client";

import {format_date, view_text, week_number_year_from_date} from "@/helper/helper";

import { Tooltip } from "react-tooltip";
import MyDate from "../TermListItem/MyDate";
const TermBar = ({ term }: { term: any }) => {
  return (
    <div className="flex  flex-col gap-x-9 sm:flex-row ">
      <div className="mt-2 flex flex-row items-center  gap-x-1 text-sm">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="h-6 w-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
          />
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
          />
        </svg>

        <span dir="ltr" className="flex gap-1 font-normal ltr:ml-3 rtl:mr-3">
          <span>{parseInt(term.term_info.view_count)}</span>{" "}
          <span> {view_text(parseInt(term.term_info.view_count))} </span>
        </span>
      </div>

      <div
        className={`tooltip_iri mt-2 flex flex-row items-center gap-x-1 text-sm`}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          viewBox="0 0 16 16"
        >
          <g fill="currentColor">
            <path d="M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v1h16V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4V.5zm5.402 9.746c.625 0 1.184-.484 1.184-1.18c0-.832-.527-1.23-1.16-1.23c-.586 0-1.168.387-1.168 1.21c0 .817.543 1.2 1.144 1.2z" />
            <path d="M16 14V5H0v9a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2zm-6.664-1.21c-1.11 0-1.656-.767-1.703-1.407h.683c.043.37.387.82 1.051.82c.844 0 1.301-.848 1.305-2.164h-.027c-.153.414-.637.79-1.383.79c-.852 0-1.676-.61-1.676-1.77c0-1.137.871-1.809 1.797-1.809c1.172 0 1.953.734 1.953 2.668c0 1.805-.742 2.871-2 2.871zm-2.89-5.435v5.332H5.77V8.079h-.012c-.29.156-.883.52-1.258.777V8.16a12.6 12.6 0 0 1 1.313-.805h.632z" />
          </g>
        </svg>
        <span dir="ltr" className="flex gap-1 font-normal ltr:ml-3 rtl:mr-3">
          <span>
          Week  { week_number_year_from_date(term.date.$date)}
          </span>
        </span>
      </div>

      <Tooltip
        anchorSelect={`.tooltip_iri`}
        className="my_tooltip"
        place="bottom"
        content={
          "The week number / year: " +
          week_number_year_from_date(term.date.$date)

        }
      />
    </div>
  );
};

export default TermBar;
