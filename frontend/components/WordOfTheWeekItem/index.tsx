import Link from "next/link";
import { toSentenceCase } from "@/helper/helper";
import TermBar from "./TermBar";
import {MouseEventHandler} from "react";
const WordOfTheWeekItem = (
    { term, movePrevious, moveNext, currentIndex, totalIndex }: {
             moveNext: MouseEventHandler<HTMLButtonElement>,
             movePrevious: MouseEventHandler<HTMLButtonElement>,
             term: any,
             currentIndex: number,
             totalIndex: number,
    }) => {

  return (
    <>
      {term && (
        <div
          className="tooltip_word_of_the_week flex justify-start rounded-lg
             bg-info/[.2] shadow-sm
                   hover:bg-slate-100 
                   group-hover:opacity-90
                    sm:mx-10
                    border-2 border-primary
                    "
        >
            <div className={`flex items-center ${
                  currentIndex === (totalIndex - 1)  ? "invisible" : "visible"
                } `}>
              <button
                  type="button"
                  className="flex items-center rounded px-1 py-3 text-center text-base font-medium text-primary hover:text-info"
                  onClick={movePrevious}
              >
                 <span className="flex flex-col items-center">

                <span className="text-6xl">
                 <svg className="w-16 h-16 " aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" viewBox="0 0 24 24">
                  <path fillRule="evenodd"
                        d="M13.729 5.575c1.304-1.074 3.27-.146 3.27 1.544v9.762c0 1.69-1.966 2.618-3.27 1.544l-5.927-4.881a2 2 0 0 1 0-3.088l5.927-4.88Z"
                        clipRule="evenodd"/>
                </svg>
                </span>
                   <span className="text font-semibold md:text-base">
                  Previous
                </span>
              </span>
              </button>
            </div>

            <Link href={`/term/${term.url_slug}`} className="flex flex-auto flex-col basis-full py-4 px-2 leading-normal ">
              <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
                <span>Word of the week</span>
              </h2>
              <hr className="mx-auto my-1 h-1 w-48 rounded border-0 bg-gray-200 md:my-1 "></hr>
              <h2 className="break-all p-2 text-xl font-bold   ">
                <span className="text-primary">{toSentenceCase(term.label)}</span>
              </h2>

              <div className="flex flex-col flex-wrap">
                <h5 className="px-2 font-normal ">{term.definition}</h5>
              </div>
              <div className="flex justify-between">
                <p className="p-2 pt-4 text-xs">
                  View the full entry here
                </p>
                <div className="flex justify-end">
                  <TermBar term={term}/>
                </div>
              </div>
            </Link>

            <div className={`flex items-center ${
                  currentIndex === 0  ? "invisible" : "visible"
                }`}>
              <button
                  type="button"
                  className="flex items-center rounded px-1 py-3 text-center text-base font-medium text-primary hover:text-info"
                  onClick={moveNext}
              >
                 <span className="flex flex-col items-center">

                <span className="text-6xl">
                <svg className="w-16 h-16" aria-hidden="true"
                     xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" viewBox="0 0 24 24">
                  <path fillRule="evenodd"
                        d="M10.271 5.575C8.967 4.501 7 5.43 7 7.12v9.762c0 1.69 1.967 2.618 3.271 1.544l5.927-4.881a2 2 0 0 0 0-3.088l-5.927-4.88Z"
                        clipRule="evenodd"/>
                </svg>

                </span>
                   <span className="text font-semibold md:text-base">
                  Next
                </span>
              </span>
              </button>
            </div>
        </div>
      )}
    </>
  );
};

export default WordOfTheWeekItem;
