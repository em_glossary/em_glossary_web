import Markdown from "react-markdown";
import rehypeExternalLinks from "rehype-external-links";

const AdoptersMessage = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div
          className="m-2 mt-6 flex flex-col justify-start
       p-2 text-primary sm:mx-12 md:flex-row"
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
              <span>See where EM Glossary terminology is adopted</span>
            </h2>

            <div className="flex flex-col flex-wrap text-start">
              <Markdown
                rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
              >{`The EM Glossary initiative strives to provide stable, domain-level terminology to be used as a harmonization and alignment target for independent application-level developments. Our [documentation](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home) provides detailed examples and recommendations on how to adopt. Here you can quickly check out who is adopting our terminology and where.`}</Markdown>
              <div className="py-3">
                <Markdown
                  rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
                >{`We are always happy to support adoption or simply to feature stories here. [Email us](mailto:hmc@fz-juelich.de) to learn more.`}</Markdown>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdoptersMessage;
