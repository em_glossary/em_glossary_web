"use client";

import Link from "next/link";

import { ALPHABET_LIST } from "../../helper/const";
import { Tooltip } from "react-tooltip";
import { tooltip_text } from "@/helper/tooltips_text";

const Sidebar = () => {
  return (
    <>
      <aside
        className="no-print flex min-h-full basis-full flex-col 
rounded-lg md:basis-1/4 md:border-r-2 md:border-dashed lg:basis-1/4"
      >
        <div className="flex flex-col  rounded-lg border-b-2 border-dashed p-2">
          <h1 className="tooltip_browse_alphabetically pb-2 text-center text-2xl font-semibold">
            Browse alphabetically
          </h1>
          <Tooltip
            className="my_tooltip"
            anchorSelect={`.tooltip_browse_alphabetically`}
            place="bottom"
            content={tooltip_text["browse_alphabetically"]}
          />
          <div className="flex justify-center">
            <div className="flex flex-row flex-wrap justify-center">
              {ALPHABET_LIST.map((item) => {
                if (item === "All") {
                  return (
                    <Link
                      href={`/terms?page=1`}
                      key={`tooltip_link_${item}`}
                      className={`tooltip_all m-px h-11 w-11 rounded-lg bg-primary p-2 text-center
                   text-base text-white hover:bg-info
                   focus:bg-fucus
                    group-hover:opacity-70`}
                    >
                      {item}
                      <Tooltip
                        anchorSelect={`.tooltip_all`}
                        className="my_tooltip"
                        key={`tooltip_value_${item}`}
                        place="bottom"
                        content={tooltip_text["show_all_terms"]}
                      />
                    </Link>
                  );
                } else {
                  return (
                    <Link
                      href={"/terms/" + String(item).toLowerCase()}
                      key={`tooltip_link_${item}`}
                      className={`tooltip_${item} m-px h-11 w-11 rounded-lg bg-primary p-2 text-center
                   text-base text-white hover:bg-info 
                   focus:bg-fucus
                    group-hover:opacity-70`}
                    >
                      {item}
                      <Tooltip
                        className="my_tooltip"
                        anchorSelect={`.tooltip_${item}`}
                        key={`tooltip_value_${item}`}
                        place="bottom"
                        content={`${tooltip_text["letter_buttons"]} (${item})`}
                      />
                    </Link>
                  );
                }
              })}
            </div>
          </div>
        </div>
      </aside>
    </>
  );
};

export default Sidebar;
