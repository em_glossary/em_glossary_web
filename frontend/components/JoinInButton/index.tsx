import Link from "next/link";

const JoinInButton = () => {
  return (
    <>
      <Link
        href={`/about`}
        className="m-2 flex flex-col justify-start rounded-lg border-2 
        border-primary bg-info/[.05] py-6  shadow-sm
                     hover:bg-slate-100 
                     group-hover:opacity-90
                       md:flex-row 
                      "
      >
        <div className="flex flex-auto flex-col  p-4 leading-normal ">
          <h2 className="break-all p-2 text-center text-3xl  font-bold">
            <span>JOIN IN</span>
          </h2>

          <p className="p-2 pb-3 text-center text-base">
            Learn about who we are, how we collaborate, and how you can join in.
          </p>

          <div className="flex justify-center">
            <button
              type="button"
              className="inline-flex items-center rounded bg-primary px-5 py-3 text-center text-base font-medium text-white hover:bg-info"
            >
              Learn more
              <svg
                className="ms-2 h-6 w-6 "
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeWidth="2"
                  d="M4.5 17H4a1 1 0 0 1-1-1 3 3 0 0 1 3-3h1m0-3.05A2.5 2.5 0 1 1 9 5.5M19.5 17h.5a1 1 0 0 0 1-1 3 3 0 0 0-3-3h-1m0-3.05a2.5 2.5 0 1 0-2-4.45m.5 13.5h-7a1 1 0 0 1-1-1 3 3 0 0 1 3-3h3a3 3 0 0 1 3 3 1 1 0 0 1-1 1Zm-1-9.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0Z"
                />
              </svg>
            </button>
          </div>
        </div>
      </Link>
    </>
  );
};

export default JoinInButton;
