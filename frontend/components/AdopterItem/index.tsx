import Image from "next/image";
import Markdown from "react-markdown";
import rehypeExternalLinks from "rehype-external-links";
const AdopterItem = ({ item }: { item: any }) => {
  return (
    <>
      {item && (
        <div
          className="contributor m-2 flex flex-col justify-start rounded-lg border bg-white py-4 shadow-sm
           hover:bg-gray-100 md:flex-row"
        >
          <Image
            className="h-96 w-full rounded-t-lg object-contain p-4 md:h-auto md:w-48 md:rounded-none md:rounded-s-lg"
            src={`/images/${item.logo != "" ? item.logo : "default.jpg"}`}
            alt={item.name}
            width={300}
            height={300}
          />

          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <a target="_blank" rel="noopener noreferrer" href={`${item.url}`}>
              <h2 className="break-all p-2 text-lg font-bold  text-gray-900  ">
                <span
                  className="bg-gradient-to-r from-primary to-info bg-clip-text text-transparent 
               "
                >
                  {item.name}
                </span>
              </h2>
            </a>

            <div className="flex flex-col flex-wrap py-2 text-left" dir="ltr">
              <div className="px-2 font-normal text-primary ">
                <Markdown
                  rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
                >
                  {item.info}
                </Markdown>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default AdopterItem;
