"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { ColorRing } from "react-loader-spinner";
import { Tooltip } from "react-tooltip";
import { tooltip_text } from "@/helper/tooltips_text";

const Statistics = () => {
  const [data, setData] = useState<any>({});
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(process.env.NEXT_PUBLIC_BACKEND_URL + "api/statistics")
      .then((data) => {
        // console.log(data.data);
        setData(data.data);
        setLoading(false);
      });
  }, []);

  return (
    <>
      {isLoading && (
        <ColorRing
          visible={true}
          height="80"
          width="80"
          ariaLabel="blocks-loading"
          wrapperStyle={{}}
          wrapperClass="blocks-wrapper"
          colors={["#e15b64", "#f47e60", "#f8b26a", "#abbd81", "#849b87"]}
        />
      )}
      {data && data.total_terms && (
        <div className="no-print container mx-auto">
          <div className="mx-auto px-4 py-4 sm:max-w-xl md:max-w-full md:px-4 lg:max-w-screen-xl lg:px-2 lg:py-20">
            <div className="row-gap-2 grid gap-2 sm:grid-cols-3">
              <div className="tooltip_total_term text-center">
                <h6 className="text-deep-purple-accent-400 text-5xl font-bold">
                  {data.total_terms}
                </h6>
                <p className="font-bold">Total Terms</p>
              </div>
              <Tooltip
                className="my_tooltip"
                anchorSelect={`.tooltip_total_term`}
                place="bottom"
                content={tooltip_text["total_terms"]}
              />

              <div className="text-center">
                <h6 className="text-deep-purple-accent-400 text-5xl font-bold">
                  {data.max_date}
                </h6>
                <p className="font-bold">Last Modification Date</p>
              </div>

              <div className="text-center">
                <h6 className="text-deep-purple-accent-400 text-5xl font-bold">
                  {data.total_views}
                </h6>
                <p className="font-bold">Total Views of Terms</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Statistics;
