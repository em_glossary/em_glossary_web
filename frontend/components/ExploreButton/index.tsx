import Link from "next/link";

const ExploreButton = () => {
  return (
    <>
      <Link
        href={`/explore`}
        className="m-2 flex flex-col justify-start rounded-lg border-2 border-primary  bg-info/[.05] py-6 
            shadow-sm hover:bg-slate-100 group-hover:opacity-90 md:flex-row"
      >
        <div className="flex flex-auto flex-col  p-4 leading-normal ">
          <h2 className=" break-all p-2 text-center text-3xl font-bold">
            <span>EXPLORE</span>
          </h2>

          <p className="p-2 pb-3 text-center text-base">
            Explore the glossary terms by alphabetic browsing or keyword
            search.
          </p>

          <div className="flex justify-center">
            <button
              type="button"
              className="inline-flex items-center rounded bg-primary px-5 py-3 text-center text-base font-medium text-white hover:bg-info"
            >
              Discover terms
              <svg
                className="ms-2 h-4 w-4 "
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                />
              </svg>
            </button>
          </div>
        </div>
      </Link>
    </>
  );
};

export default ExploreButton;
