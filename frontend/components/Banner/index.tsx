import Image from "next/image";
import Link from "next/link";
const Banner = () => {
  return (
    <>
      <div className="no-print mt-14 h-64  bg-primary bg-[url('/images/banner.jpg')] bg-cover bg-scroll bg-center">
        <div className="container mx-auto  content-center">
          <div
            className="flex flex-col flex-wrap  items-stretch justify-between rounded-lg 
         sm:mx-12 sm:my-20 sm:flex-row"
          >
            <h1 className="pt-9 text-center align-middle text-3xl font-semibold text-body-color">
              Electron Microscopy Glossary
            </h1>

            <div className="flex flex-row justify-center align-middle">
              <Link href={`/`}>
                <Image
                  src="/images/glossary_log_no_bg.png"
                  alt="logo"
                  width={140}
                  height={30}
                  className="h-auto w-52 "
                />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Banner;
