import { Menu } from "@/types/menu";

const menuData: Menu[] = [
  {
    id: 1,
    title: "Home",
    path: "/",
    newTab: false,
  },
  {
    id: 2,
    title: "Explore",
    path: "/explore",
    newTab: false,
  },
  {
    id: 3,
    title: "Understand",
    path: "/understand",
    newTab: false,
  },
  {
    id: 4,
    title: "Contributors",
    path: "/contributors",
    newTab: false,
  },

  {
    id: 5,
    title: "Adopters",
    path: "/adopters",
    newTab: false,
  },
  {
    id: 6,
    title: "About",
    path: "/about",
    newTab: false,
  },
  // {
  //   id: 3,
  //   title: "Contributors",
  //   newTab: false,
  //   submenu: [
  //     {
  //       id: 41,
  //       title: "Contributors Design 1 (Main)",
  //       path: "/contributors",
  //       newTab: false,
  //     },
  //     {
  //       id: 42,
  //       title: "Contributors Design 2 (Draft)",
  //       path: "/contributors_draft_1",
  //       newTab: false,
  //     },
];
export default menuData;
