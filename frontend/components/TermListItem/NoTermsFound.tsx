import Markdown from "react-markdown";
import rehypeExternalLinks from "rehype-external-links";
const NoTermsFound = () => {
  return (
    <div className="m-12 flex  flex-col gap-9 sm:flex-row ">
      <div className="mt-2 flex items-center   text-sm text-primary">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="h-20 w-20"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
          />
        </svg>

        <div className="ml-2 font-bold ">
          <Markdown
            rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
          >
            {`There aren't any terms starting with this letter yet. If you think
            we are missing something why not open an issue suggesting a term on
            our
            [GitLab](https://codebase.helmholtz.cloud/em_glossary/em_glossary)
            repository or send an [email](mailto:hmc-matter@helmholtz-berlin.de)
            with your suggestion?`}
          </Markdown>
        </div>
      </div>
    </div>
  );
};

export default NoTermsFound;
