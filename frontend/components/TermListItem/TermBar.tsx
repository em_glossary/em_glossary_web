"use client";

import { format_date, view_text } from "@/helper/helper";
import MyDate from "./MyDate";
import { Tooltip } from "react-tooltip";
import { tooltip_text } from "@/helper/tooltips_text";
const TermBar = ({ term }: { term: any }) => {
  return (
    <div className="flex  flex-col gap-x-9 sm:flex-row ">
      <div className="tooltip_view mt-2 flex flex-row items-center  gap-x-1 text-sm text-gray-500">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="h-6 w-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
          />
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
          />
        </svg>

        <span dir="ltr" className="flex gap-1 font-normal ltr:ml-3 rtl:mr-3">
          <span>{parseInt(term.view_count)}</span>{" "}
          <span> {view_text(parseInt(term.view_count))} </span>
        </span>
      </div>
      <Tooltip
        className="my_tooltip"
        anchorSelect={`.tooltip_view`}
        place="bottom"
        content={tooltip_text["view"]}
      />

      <div className="mt-2 flex flex-row items-center gap-x-1 text-sm text-gray-500">
        <svg
          className="h-6 w-6  "
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M5 10V7.914a1 1 0 0 1 .293-.707l3.914-3.914A1 1 0 0 1 9.914 3H18a1 1 0 0 1 1 1v6M5 19v1a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-1M10 3v4a1 1 0 0 1-1 1H5m14 9.006h-.335a1.647 1.647 0 0 1-1.647-1.647v-1.706a1.647 1.647 0 0 1 1.647-1.647L19 12M5 12v5h1.375A1.626 1.626 0 0 0 8 15.375v-1.75A1.626 1.626 0 0 0 6.375 12H5Zm9 1.5v2a1.5 1.5 0 0 1-1.5 1.5v0a1.5 1.5 0 0 1-1.5-1.5v-2a1.5 1.5 0 0 1 1.5-1.5v0a1.5 1.5 0 0 1 1.5 1.5Z"
          />
        </svg>

        <span dir="ltr" className="flex gap-1 font-normal ltr:ml-3 rtl:mr-3">
          <span>v1.0.0</span>
        </span>
      </div>
    </div>
  );
};

export default TermBar;
