const AboutMessage = () => {
  return (
    <>
      <div className="container mx-auto">
        <div
          className="m-2 mt-6 flex flex-col justify-start
       p-2 text-primary sm:mx-12 md:flex-row"
        >
          <div className="flex flex-auto flex-col  p-4 leading-normal ">
            <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
              <span>Learn about the EM Glossary initiative</span>
            </h2>

            <div className="flex flex-col flex-wrap text-center">
              <p>{`Here you will find additional information about how the EM Glossary came to life, how it is maintained, how we work together, and how you can join in. `}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutMessage;
