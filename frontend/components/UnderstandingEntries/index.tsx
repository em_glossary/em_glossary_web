import Link from "next/link";

const UnderstandingEntries = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div className="">
          <Link
            href={`/understand`}
            className="m-2 mt-6 flex flex-col justify-start
             p-2 text-primary 
        hover:bg-slate-100 group-hover:opacity-90 sm:mx-12 
        md:flex-row"
          >
            <div className="flex flex-auto flex-col  p-4 leading-normal ">
              <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
                <span>Explore the glossary</span>
              </h2>

              <div className="flex flex-col flex-wrap text-center">
                <p>
                  {`For further information about what 
                  is displayed on these pages please visit our
                  Understand page`}
                </p>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </>
  );
};

export default UnderstandingEntries;
