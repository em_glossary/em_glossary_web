import Link from "next/link";
import Markdown from "react-markdown";
import rehypeExternalLinks from "rehype-external-links";

const TopMessageUnderstandingPage = () => {
  return (
    <>
      <div className="no-print container mx-auto">
        <div className="">
          <div
            className="m-2 mt-6 flex flex-col justify-start
             p-2 text-primary 
        hover:bg-slate-100 group-hover:opacity-90 sm:mx-12 
        md:flex-row"
          >
            <div className="flex flex-auto flex-col  p-4 leading-normal ">
              <h2 className="text- break-all p-2 text-center text-2xl  font-bold">
                <span>Understanding entries</span>
              </h2>

              <div className="flex flex-col flex-wrap text-justify">
                <Markdown
                  rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
                >
                  {`When you browse the EM Glossary you will notice that each term consists of structured data in the form of key-value pairs. 
Most of the keys can be understood intuitively. However they do have a specific technical purpose. The keys that we use were chosen as they can be well represented
(either as class hierarchy or as annotation properties)
 in the web ontology language (OWL). This is necessary
 for the technical implementation of the glossary.
 You can find examples of the glossary implementation
  on our [adopters page](/adopters).`}
                </Markdown>

                <Markdown
                  className="pt-2"
                  rehypePlugins={[[rehypeExternalLinks, { target: "_blank" }]]}
                >
                  {`Here we provide additional information on how we understand and use the different keys for further disambiguation.`}
                </Markdown>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TopMessageUnderstandingPage;
