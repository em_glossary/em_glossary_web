import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "bg-banner": "url('../public/Banner.png')",
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontFamily: {
        halvar: ["var(--font-halvar)"],
      },
      colors: {
        blue: "#002864",
        current: "currentColor",
        transparent: "transparent",
        white: "#FFFFFF",
        black: "#1c1917",
        dark: "#1c1917",
        info: "#14C8FF",
        fucus: "#14C8FF",
        primary: "#002864",
        secondary: "#005AA0",
        "body-color": "#FFFFFF",
      },
    },
  },
  plugins: [],
};
export default config;
