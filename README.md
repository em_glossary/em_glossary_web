# EM Glossary - _Web_

<img src="https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/raw/main/EMGlossary.png" alt="EM Glossary Group Logo" width="400"/>


# Welcome to the EM-Glossary

We work towards harmonized metadata in electron microscopy!

Visit our [webpage](emglossary.helmholtz-metadaten.de) and start developing with us in our [community repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary)!


This is where the code for the EM-Glossary Explorer is developed and documented. We offer electron microscopy terminology that was harmonized through a community process - the EM-Glossary Explorer offers a convenient way to browse the content of the glossary in a web-based representation. 
It is intended for scientific and academic usage through web technology via small to large devices (Mobile, Tablet, Laptop and large screen PC or projector).

### Further information in our wiki:

In our [wiki](https://codebase.helmholtz.cloud/em_glossary/em_glossary_web/-/wikis/Home) we document: 
- technologies used for EM-Glossary Web
- installation, prerequisites and dependencies
For more information regarding the EM Glossary initiative and its scope, please visit our [webpage](emglossary.helmholtz-metadaten.de). For more information regarding the EM Glossary OWL implmentation visit our [EM Glossary OWL repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl), or our [EM Glossary OWL documentation page](owl.emglossary.helmholtz-metadaten.de).


### Please cite this repository as

Sedeqi, M. R., Özkan, Ö., Brendike-Mannix, O., Hofmann, V. (2024), Electron Microscopy Glossary Web, emglossary.helmholtz-metadaten.de

## License

The `EM Glossary Web` are licensed under the [MIT License](https://codebase.helmholtz.cloud/em_glossary/em_glossary_web/-/blob/development/LICENSE.txt?ref_type=heads);
you may not use this file except in compliance with the License.
You may obtain a copy of the License in the LICENSE in this repository.

### Funding

This work was supported by the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative, specifically HMC Hub Information at the [Institute for Advanced Simulation - Materials Data Science and Informatics (IAS-9)](https://www.fz-juelich.de/de/ias/ias-9) at [Forschungszentrum Jülich GmbH (FZJ)](https://ror.org/02nv7yv05), and HMC Hub Matter at the Abteilung Experimentsteuerung und Datenerfassung at the [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28).

<p align="center">
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_FZJ" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/FZJ/Logo_FZ_Juelich_412x120_rgb_jpg.jpg?raw=true" width="200">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="200">
</p>

### Acknowledgements according to [CRediT](https://credit.niso.org/):

With respect to [EM Glossary project](https://codebase.helmholtz.cloud/em_glossary/), the following individuals are mapped to [CRediT](https://credit.niso.org/) (alphabetical order): \
[Abril Azocar Guzman](https://orcid.org/0000-0001-7564-7990) (AAG); [Mojeeb Rahman Sedeqi](https://orcid.org/0000-0002-9694-0122) (MRS); [Özlem Özkan](https://orcid.org/0000-0003-1965-7996) (ÖÖ); [Oonagh Brendike-Mannix](https://orcid.org/0000-0003-0575-2853)(OBM); [Stefan Sandfeld](https://orcid.org/0000-0001-9560-4728) (StS); [Volker Hofmann](https://orcid.org/0000-0002-5149-603X) (VH);

Contributions according to CRediT are: \
[Conceptualization](https://credit.niso.org/contributor-roles/conceptualization/): OBM, StS, VH; [Data curation](https://credit.niso.org/contributor-roles/data-curation/): AAG, ÖÖ, OBM, VH; [Funding acquisition](https://credit.niso.org/contributor-roles/funding-acquisition/): OBM, StS, VH; [Methodology](https://credit.niso.org/contributor-roles/methodology/); AAG, MRS, AAG, VH; [Project administration](https://credit.niso.org/contributor-roles/project-administration/): ÖÖ, OBM, VH; [Resources](https://credit.niso.org/contributor-roles/software/): StS, VH; [Software](https://credit.niso.org/contributor-roles/software/): AAG, MRS, VH; [Supervision](https://credit.niso.org/contributor-roles/supervision/): OBM, StS, VH; [Visualization](https://credit.niso.org/contributor-roles/visualization/): AAG, MRS, ÖÖ, OBM, VH;
