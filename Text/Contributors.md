# Text for Contributors Page 
This file contains text that should be implemented on contributors page according to [Mockup](https://app.mural.co/t/hmcoonagh3653/m/hmcoonagh3653/1710759143862/7050bd26f3bd1813079bc193baa237aa19b18e29?sender=u4188ea98ee41e27cb2ca8580)
---

### Find out who we are:
The EM Glossary is a community driven collaboration of scientists and domain experts from electron microscopy with experts from the fields of metadata and information engineering. With our combined expertise, we work jointly work to provide harmonized, high quality, well implemented and stable terminology for electron microscopy. Check out who contributed: 


** Contribuors information according to Contributors.yaml**