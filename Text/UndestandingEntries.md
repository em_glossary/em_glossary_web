# Text for "understanding entries" page
This file contains text that should be implemented on landing page according to [Mockup](https://app.mural.co/t/hmcoonagh3653/m/hmcoonagh3653/1710759143862/7050bd26f3bd1813079bc193baa237aa19b18e29?sender=u4188ea98ee41e27cb2ca8580)
--- 

## This is the main text on the page top

### Understanding entries
When you browse the EM Glossary you will notice that each term consists of structured data in the form of key-value pairs. 

Most of the keys can be understood intuitively. However they do have a specific technical purpose. The keys that we use were chosen as they can be well represented (either as class hierarchy or as annotation properties) in the web ontology language (OWL). This is necessary for the technical implementation of the glossary. You can find examples of the glossary implementation on our [adopters page](https://emglossary.helmholtz-metadaten.de/adopters).
Here we provide additional information on how we understand and use the different keys for further disambiguation.

Please note, not all keys appear on each term page. We have defined four mandatory keys: Label, Definition, IRI, and Contributors; these appear with every term.


## These are the entries in the lower boxes on the left

### Label (*mandatory*):
The human-readable, most commonly used name for the term. In the EM Glossary OWL implementation this is used to name each class.

### Definition (*mandatory*): 
This phrase describes the meaning behind the Label. It is designed for human understanding. 
For clarity and brevity our definitions follow two principles :
1. We use the Aristotelian [Genus-Differentia form](https://en.wikipedia.org/wiki/Genus%E2%80%93differentia_definition) for writing definitions. As such our definitions start with a genus, A,  followed by a list of differentia B1, B2... Bn. Differentia are features which differentiate this particular term from other terms of the same genus. This results in a definition in the form: term is an A which B1, B2... Bn. 

In our OWL implementation the genus gets mapped to the class hierarchy, and a subclass in the hierarchy inherits all properties from its superclass.
2. We unpack definitions. Where definitions require subject-specific technical terms, we use them, but ensure that they are defined elsewhere in the glossary as well. The result is a strict one-term-per-definition policy. While browsing the glossary you will find and quick-links to these related terms and their definitions, as well as an entry under the Internal Reference key.

### Contributors (*mandatory*)
A list which identifies the persons that contributed to discussions and drafting of the content associated with each term. The links in the term resolve to [ORCID](https://orcid.org/) which are persistent identifiers for people. More information on our contributors can be found in the (contributors page)[https://emglossary.helmholtz-metadaten.de/contributors].

### IRI (*mandatory*)
Stands for [Internationalized Resource Identifier](https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier) - a persistent, unique way of identifying the web location where a term's information is stored. It is important for the machine readability of the OWL implementation of the EM Glossary. The link will forward you to the documentation page of the EM Glossary OWL artifact.

### Commments: 
Contains additional information about a term. When writing the definition this information, although important, was not considered pertinent to the features that differentiate this particular term, and therefore not included in the definition's differentia. Comments further enrich a term description with additional context.

### Acronyms: 
A form of representing the term by its initials (or other letters), to be pronounced like a word, and be used verbally or in the literature. An example is IRI for International Resource Identifier.

###  Abbreviations 
A shortened form of the word or phrase used as the term's Label. The shorter form is often used as a shortcut when writing.

### Sources
A list of external resources used during the discussion and drafting process of the Definition, and other term information. 

### Examples    
A list of examples which are thought to illustrate the use of, or give context to, a term; thereby aiding practical application and deepening comprehension. An Example for a person would be John Doe.

### Internal reference
A list of terms that are used in the Definition or Comment, and which are defined and used elsewhere in the EM Glossary. By clicking the links, you can easily navigate to related terminology in this glossary.

### Synonyms
Exact Synonyms: A list of synonyms that are entirely interchangeable with the term Label. The Definitions of the term Label and its Exact Synonyms are identical. 
     
Narrow Synonyms: A list of synonyms that share meaning with the term Label, but are more specific and represent a more limited concept. The term Label and its Narrow Synonyms are not interchangeable. The term Label will have meanings that are not covered by the Narrow Synonyms.
      
Broad Synonyms: A list of synonyms that share meaning with the term Label, but are less specific and represent a more general concept. The term Label and its Broad Synonyms are not interchangeable. The Broad Synonym will have meanings not covered by the Label.

Related Synonyms: A list of synonyms that share partial meaning with the term Label. The term Label and its Related Synonyms are not interchangeable. Each have distinct meanings, but also share some overlap in their meaning.
