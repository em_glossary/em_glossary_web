Text for "About" page
This file contains text that should be implemented on the about page according to [Mockup](https://app.mural.co/t/hmcoonagh3653/m/hmcoonagh3653/1710759143862/7050bd26f3bd1813079bc193baa237aa19b18e29?sender=u4188ea98ee41e27cb2ca8580)
--- 

Page Heading: Learn about the EM Glossary initiative
Page Subheading: Here you will find additional information about how the EM Glossary came to life, how it is maintained, how we work together, and how you can join in. 



## The problem
Terminology in the field of electron microscopy is heterogeneous: as a rapidly developing field its language developed through conversations in labs, terminology coined in scientific articles and books, and labels on devices and user interfaces. Any standardization has occurred informally. 

When heterogeneous terminology meets a multiplicity of agents (human, device, algorithm) the potential for miscommunication is high, and the potential for data interoperability is low. [Interoperability](https://purls.helmholtz-metadaten.de/hob/HDO_00007010): the potential for two independent agents to work on the same data in a coordinated manner - is necessary for succesful collaboration.

By harmonizing terminology we increase semantic interoperability; thereby reducing semantic ambiguity. In other words we enable better collaboration between humans, devices, and algorithms in electron microscopy.

## Our approach
We follow a four-prong approach to harmonize terminology used by both humans and machines:

1. Text: A broad community-driven process of terminology harmonization
   Button Text: Visit our community repository 
   Link to: https://codebase.helmholtz.cloud/em_glossary/em_glossary
2. Text: A machine readable implementation according to semantic web standards
   Button: See our OWL artifact
   Link to: https://owl.emglossary.helmholtz-metadaten.de/
3. Text: Convenient exploration of the terminology through a web-app
   Button Text: Explore the EM Glossary
   Link to: Explore page
4. Text: Support and counseling for application-level adoption
   Button Text: See EM Glossary adopters
   Link to: Adopters page

## About the EM Glossary Initiative 
The EM Glossary Initiative combines the expertise of domain scientists in electron microscopy with that of professionals in metadata and information engineering to harmonize terminology in electron microscopy. We do this by providing clear definitions of key terms used in the field. These are created through a moderated co-design process in regular meetings.

Our goal is to improve interoperability within electron microscopy, and between electron microscopy and its neighboring fields. The EM Glossary provides stable, domain-level semantics for adoption by application-level developments - consider a metadata schema describing a measurement, or the metadata used for an analysis program. This stable, harmonized terminology also facilitates semantic crosswalks to applications and artifacts used in neighboring fields. In this way adopting or aligning with EM Glossary terminology improves both intra- and interdisciplinary interoperability of your data.

We aim at providing stable semantics - the resource is however developed continuously. It is created in a not-for-profit collaboration between domain scientists, method experts, knowledge engineers, and metadata stewards. With our implementation of the results we strive to balance the needs of these stakeholders and provide the EM Glossary Explorer (for convenient browsing by domain scientists) and the machine readible OWL artifact (for knowledge engineers and technical implementation). As you explore the initiative, you will find many weblinks between the Explorer pages and the OWL artifact. These weblinks are the connections between the worlds of the laboratory and information systems, and of our community taking both these needs into account.

## Use EM Glossary terminology

In case you find the idea of technically implementing a glossary daunting or puzzling, don't worry. Our [adopters page](https://emglossary.helmholtz-metadaten.de/adopters) provides examples of how our terminology is adopted technically. More detailed examples, along with recommendations are in the [documentation](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home) of our OWL artifact. Citation guidelines are contained in the OWL artifact under the key 'Cite as'. We are always interested in learning how the glossary is used so don't hesitate to let us know and we can feature your use case.

There is scientific value in the glossary itself. We are very happy for you to use the definitions in scientific activities, such as writing a paper or teaching. On the individual term pages you will find citation suggestions for individual terms. Please tell us if you use the glossary in this way, it will be a big milestone for our community!

The code for the EM Glossary Explorer is [publicly available](https://codebase.helmholtz.cloud/em_glossary/em_glossary_web). Citation information is contained in the repo. 

Contributions to the glossary are logged in a micro-crediting system using [ORCID](https://orcid.org/) and credited on each [individual term page](https://emglossary.helmholtz-metadaten.de/understand) and OWL class. We recognize that every contribution counts and are very grateful to the time the community invests in the development of the terminology. This is the origin of the complicated citation structure - citations for the OWL artifact, individual terms, and the code of the EM Glossary Explorer differ. If you have questions don't hesitate to ask, we try to make citation as intuitive as possible. 

## Feedback Welcome / Join us!

The EM Glossary is the product of a collaborative effort of [volunteers](https://emglossary.helmholtz-metadaten.de/contributors). Whether your expertise is in electron microscopy, metadata, or semantics you are more than welcome to provide feedback or join the initiative and contribute to the community. Consider:
- requesting a new term or changes to an existing one by creating an issue in our [GitLab repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/issues) 
- writing a comment on an existing issue in our [GitLab repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/issues)
- join one of our regular development meetings [email us](mailto:hmc@fz-juelich.de) for details. These take place on alternating Mondays at 1:30 pm (CEST, UTC +02:00). 
All contributions are logged in our micro-crediting system using [ORCID](https://orcid.org/). We value your input.


If you want to stay informed (and potentially join in later) you can:
- [join](https://lists.fz-juelich.de/mailman/listinfo/emglossary) our mailing list (mails go out approx four times a month)
- follow on [mastodon](https://helmholtz.social/@helmholtz_hmc)/[LinkedIn](https://www.linkedin.com/company/helmholtz-metadata-collaboration-hmc/)/[X](https://twitter.com/helmholtz_hmc) and meet us at a conference. 

## About the current version 
The EM Glossary aims to provide stable semantics, but is a living work in progress: updates and releases do not follow a fixed schedule but will occur about 2-4 times per year based on our contributors reaching a consensus on definitions. The Glossary is technically maintained by the Helmholtz Metadata Collaboration. Releases are made to [EMG.OWL](https://owl.emglossary.helmholtz-metadaten.de/) which also contains metadata regarding semantic versioning. Additional information on release cycle, procedures and change management can be found [here](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home).

## Funding support
The content development and implementation of this glossary was financially supported by the Helmholtz Metadata Collaboration (HMC), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative, specifically HMC Hub Information at the Institute for Advanced Simulation - Materials Data Science and Informatics (IAS-9) at Forschungszentrum Jülich GmbH (FZJ), and HMC Hub Matter at the Abteilung Experimentsteuerung und Datenerfassung at the Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB) and the NFDI MatWerk consortium funded by the Deutsche Forschungsgemeinschaft (DFG,German Research Foundation) under the National Research Data Infrastructure – NFDI 38/1 – project number 460247524. 

Content development was further supported by the NFDI FAIRmat consortium funded by the Deutsche Forschungsgemeinschaft (DFG,German Research Foundation) under the National Research Data Infrastructure – NFDI 38/1 – project number 460197019 (FAIRmat).