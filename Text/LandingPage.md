# Text for landing page
This file contains text that should be implemented on landing page according to [Mockup](https://app.mural.co/t/hmcoonagh3653/m/hmcoonagh3653/1710759143862/7050bd26f3bd1813079bc193baa237aa19b18e29?sender=u4188ea98ee41e27cb2ca8580)
---

### welcome message: 
- large: Welcome to the Electron Microscopy Glossary Explorer
- sub: We work together to harmonise terminology in electron microscopy

### tile 1: 
- Tile1 Title: Explore
- Subtext: Explore the glossary terms by alphabetic browsing or key-word search.
- Action Box: Discover terms

### tile 2: 
- Tile2 Title: Join in
- Subtext: Learn about who we are, how we collaborate, and how you can join in.
- Action Box: Learn more

### tile 3: 
- Tile3 Title: Implement
- Subtext: Implement EM Glossary terminology in your metadata with our OWL artifact.
- Action Box: See artifact

### word of the week: 
automatically generated