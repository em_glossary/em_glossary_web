# Text for adopters page
This file contains text that should be implemented on adopters page 


### See where EM Glossary terminology is adopted
The EM Glossary initiative strives to provide stable domain level terminology to be used as an harmonization and alignment target for independent application-level developments. Our [documentation](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home) provides detailed examples and recommendations on how to adopt. Here you can quickly check out who is adopting our terminology and where.

We are always happy to support adoption or simply to feature stories here. [Contact us](mailto:hmc@fz-juelich.de) to learn more.

#### NFDI FAIRmat 
Logo: https://www.nfdi.de/wp-content/uploads/2022/11/FAIRmat_new_with-text.png
Text: The NFDI FAIRmat consortium is focused on building a research data management solution in the areas of condensed-matter physics and the chemical physics of solids. This work includes the implementation and community building around [various extensions](https://fairmat-nfdi.github.io/nexus_definitions/fairmat-cover.html) to [NeXus](https://www.nexusformat.org/). Here comprehensive domain-level ontologies [NXem](https://fairmat-nfdi.github.io/nexus_definitions/classes/contributed_definitions/NXem.html#nxem) for electron microscopy and [NXapm](https://fairmat-nfdi.github.io/nexus_definitions/classes/contributed_definitions/NXapm.html#nxapm) for atom probe tomography adopt the EM Glossary terminology via domain-specific [application definitions and base classes](https://manual.nexusformat.org/classes/index.html).

#### Helmholtz MDMC
Logo: https://connect.helmholtz-imaging.de/media/MDMC_JL-1.png
Text: The Helmholtz MDMC focuses on [correlative characterization & advanced image analysis](https://jl-mdmc-helmholtz.de/mdmc-activities/materials-characterization/), and develops application-level [metadata schemas](https://github.com/kit-data-manager/Metadata-Schemas-for-Materials-Science) for Transmission and Scanning Electron Microscopy in which EM Glossary terminology is referenced.

#### NFDI MatWerk 
Logo: https://nfdi-matwerk.de/NFDI/_processed_/a/d/csm_LOGO2-MatWerk-150_205ddb8e00.jpg
Text: [NFDI MatWerk](https://nfdi-matwerk.de/) develops application-level metadata in collaborative Infrastructure Use Cases and Participants Projects - such as [PP13: Tomography and Microstructure based modelling](https://nfdi-matwerk.de/participant-projects/pp13-tomography-and-microstructure-based-modelling) - that [align](https://github.com/kit-data-manager/Metadata-Schemas-for-Materials-Science/tree/main/SEM-FIB_Tomography) to EM Glossary terminology.

#### Platform MaterialDigital 
Logo: https://avatars.githubusercontent.com/u/63182303?s=200&v=4
Text: [Platform MaterialDigital (PMD)](https://www.materialdigital.de/about/) is a network for scientific projects that drives digitalisation processes in material science and engineering at the interface of academic research and industry. Their [PMD core ontology](https://materialdigital.github.io/core-ontology/) is modularized and extended with several [application ontologies](https://github.com/materialdigital/application-ontologies), one of which is the [microscopy onotlogy](https://w3id.org/pmd/mo) which aligns with EM Glossary terminology. 